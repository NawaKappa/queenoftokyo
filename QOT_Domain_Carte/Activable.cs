﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QOT_Domain_Carte
{
    public class Activable : CartePouvoir
    {
        public Activable(int id, string nom, string desc, int prix) : base(id, nom, desc, prix)
        { }
    }
}

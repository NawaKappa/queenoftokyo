﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QOT_Domain_Carte
{
    public class Carte
    {
        public int Id { get; set; }
        public string Nom { get; set; }
        public string Description { get; set; }
        public int Prix { get; set; }

        public Bitmap bitmap;



        public Carte()
        {
        }

        public Carte(int id, string nom, string desc, int prix)
        {
            this.Id = id;
            this.Nom = nom;
            this.Description = desc;
            this.Prix = prix;
        }

        public void Effet()
        {
            
        }

        public override string ToString()
        {
            string res = this.Nom + ";" + this.Prix +";" + this.Id;
            return res;
        }

        public Carte StringToCarte(string carte)
        {
            Carte res = new Carte();
            List<string> tmp = carte.Split(';').ToList();

            res.Nom = tmp[0];
            res.Prix = int.Parse(tmp[1]);
            res.Id = int.Parse(tmp[2]);

            return res;
        }


    }
}

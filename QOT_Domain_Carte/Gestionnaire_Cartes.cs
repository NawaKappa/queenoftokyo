﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QOT_Domain_Carte
{
    public static class Gestionnaire_Cartes
    {
        public static List<Carte> ListeCarteCachees = new List<Carte>();

        public static List<Carte> ListeCarteVisibles = new List<Carte>();


        public static void nouvelleCarte(Carte uneCarte)
        {

        }

        public static void supprimerCarte()
        {

        }

        public static void initPioche()
        {
            int counter = 0;
            string line;

            // Read the file and display it line by line.  
            System.IO.StreamReader file = new System.IO.StreamReader(@"Ressources\Cartes.txt");
            while ((line = file.ReadLine()) != null)
            {
                string[] cards = line.Split(';');
                foreach (string card in cards)
                {
                    if (cards[5] == "rien")
                    {
                        CarteAction uneCarte = new CarteAction(int.Parse(cards[0]), cards[1], cards[2], int.Parse(cards[3]));
                        Gestionnaire_Cartes.ListeCarteCachees.Add(uneCarte);
                    }
                    else if (cards[5] == "passif")
                    {
                        Passive uneCarte = new Passive(int.Parse(cards[0]), cards[1], cards[2], int.Parse(cards[3]));
                        Gestionnaire_Cartes.ListeCarteCachees.Add(uneCarte);
                    }
                    else
                    {
                        Activable uneCarte = new Activable(int.Parse(cards[0]), cards[1], cards[2], int.Parse(cards[3]));
                        Gestionnaire_Cartes.ListeCarteCachees.Add(uneCarte);
                    }
                }
                counter++;
            }

            file.Close();
        }


        public static string ToStringGCartes()
        {
            StringBuilder str = new StringBuilder();
            foreach (Carte c in Gestionnaire_Cartes.ListeCarteCachees)
            {
                str.Append(c.ToString() + ":");
            }
            str.Remove(str.Length - 1, 1);

            return str.ToString();
        }

        public static void StringToGCartes(string listDes)
        {
            List<string> tmp = listDes.Split(':').ToList();
            for (int i = 0; i < tmp.Count; i++)
            {
                Carte tmpCarte = new Carte();
                tmpCarte = tmpCarte.StringToCarte(tmp[i]);

                Gestionnaire_Cartes.ListeCarteCachees.Add(tmpCarte);

            }

        }

        public static void Shuffle<T>(this IList<T> list)
        {
            Random rng = new Random();
            int n = list.Count;
            while (n > 1)
            {
                n--;
                int k = rng.Next(n + 1);
                T value = list[k];
                list[k] = list[n];
                list[n] = value;
            }
        }


        public static string GenerateCartes()
        {
            StringBuilder res = new StringBuilder();
            List<Carte> listCartes = new List<Carte>();
            List<Carte> allUniqueCartes = new List<Carte>();
            Random rdn = new Random();

            allUniqueCartes.Add(new Carte { Nom = "Soin", Prix = 3 });
            allUniqueCartes.Add(new Carte { Nom = "Tramway", Prix = 4 });
            allUniqueCartes.Add(new Carte { Nom = "Recharge", Prix = 8 });
            allUniqueCartes.Add(new Carte { Nom = "Restauration", Prix = 3 });
            allUniqueCartes.Add(new Carte { Nom = "Evacuation", Prix = 7 });
            allUniqueCartes.Add(new Carte { Nom = "Gaz", Prix = 6 });
            allUniqueCartes.Add(new Carte { Nom = "Lanceflamme", Prix = 3 });
            allUniqueCartes.Add(new Carte { Nom = "AmiEnfants", Prix = 3 });
            allUniqueCartes.Add(new Carte { Nom = "Acide", Prix = 3 });
            allUniqueCartes.Add(new Carte { Nom = "Regeneration", Prix = 4 });

            foreach (Carte carte in allUniqueCartes)
            {
                for (int i = 0; i < 2; i++)
                {
                    Carte newCarte = new Carte();
                    newCarte.Nom = carte.Nom;
                    newCarte.Prix = carte.Prix;
                    listCartes.Add(newCarte);
                }              
            }

            listCartes.Shuffle();

            for (int i = 0; i < listCartes.Count; i++)
            {
                listCartes[i].Id = i;
            }

            foreach (Carte carte in listCartes)
            {
                res.Append(carte.ToString() + ":");
            }
            res.Remove(res.Length - 1, 1);

            return res.ToString();
        }


        public static void UpdateGCartes()
        {
            if (ListeCarteVisibles.Count < 3)
            {
                if (ListeCarteCachees.Count > 0)
                {
                    ListeCarteVisibles.Add(ListeCarteCachees[0]);
                    ListeCarteCachees.RemoveAt(0);
                }
            }
        }

    }
}

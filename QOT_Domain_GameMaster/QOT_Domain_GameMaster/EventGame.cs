﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QOT_Domain_GameMaster
{
    public static class EventGame
    {
        public static bool affichageLance = false;
        public static bool premierLance = false;


        public static bool rerollDes1 = false;
        public static bool rerollDes2 = false;
        public static bool rerollDes3 = false;
        public static bool rerollDes4 = false;
        public static bool rerollDes5 = false;
        public static bool rerollDes6 = false;

        public static bool phaseReroll = false;

        public static bool phaseDesTerminee = false;

        public static bool argJeSuisTouche = false;
        public static bool reponseSortir = false;
        public static bool jeVeuxSortir = false;
        public static bool blockFinDeTour = false;


        public static bool phaseCarte = false;
        public static bool achatCarte = false;
        public static bool achatCarte1 = false;
        public static bool achatCarte2 = false;
        public static bool achatCarte3 = false;


        public static bool venteCarte = false;
        public static bool venteCarte1 = false;
        public static bool venteCarte2 = false;
        public static bool venteCarte3 = false;

        public static bool finDeTour = false;


        public static void ResetEventGame()
        {
            affichageLance = false;
            premierLance = false;


            rerollDes1 = false;
            rerollDes2 = false;
            rerollDes3 = false;
            rerollDes4 = false;
            rerollDes5 = false;
            rerollDes6 = false;

            phaseReroll = false;

            phaseDesTerminee = false;

            argJeSuisTouche = false;
            reponseSortir = false;
            jeVeuxSortir = false;
            blockFinDeTour = false;


            phaseCarte = false;
            achatCarte = false;
            achatCarte1 = false;
            achatCarte2 = false;
            achatCarte3 = false;


            venteCarte = false;
            venteCarte1 = false;
            venteCarte2 = false;
            venteCarte3 = false;

            finDeTour = false;
        }
    }
}

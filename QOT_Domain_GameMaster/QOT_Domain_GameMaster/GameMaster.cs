﻿using QOT_Domain_Joueur;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QOT_Domain_GameMaster
{
    public static class GameMaster
    {
        public static int tourActuel = 0;
        public static int cptLanceActuel = 0;
        public static Joueur joueurActuel;
        public static Joueur joueurLocal;
        public static int PositionTabJoueurActuel = -1;

        public static Tuple<bool,Joueur> VerifPTVictoire()
        {
            foreach (Joueur leJoueur in Gestionnaire_Joueur.ListGJoueur)
            {
                if (leJoueur.PtVictoire >= 20)
                {
                    return new Tuple<bool, Joueur>(true,leJoueur);
                }
            }
            return new Tuple<bool, Joueur>(false, joueurActuel);
        }

        public static Tuple<bool, Joueur> DernierEnVie()
        {
            if(Gestionnaire_Joueur.ListGJoueur.Count == 1)
            {
               return new Tuple<bool, Joueur>(true, Gestionnaire_Joueur.ListGJoueur[0]);
            }
            else
            {
                return new Tuple<bool, Joueur>(false,joueurActuel);
            }
        }

        public static Tuple<bool, Joueur> VerifVictoire()
        {
            if (DernierEnVie().Item1)
            {
                return DernierEnVie();
            }
            else
            {
                return VerifPTVictoire();
            }
        }

        public static bool VerifNbLance()
        {
            if(cptLanceActuel == joueurActuel.ParametreJoueur.nbLance)
            {
                return false;
            }
            else
            {
                cptLanceActuel += 1;
                return true;
            }
        }

        public static void Init(List<Joueur> lesJoueurs, Joueur jLocal)
        {
            Gestionnaire_Joueur.ListGJoueur = lesJoueurs.ToList();
            PlateauDeJeu.JoueurDedans = Gestionnaire_Joueur.ListGJoueur[0];
            PlateauDeJeu.JoueursDehors = Gestionnaire_Joueur.ListGJoueur.ToList();
            PlateauDeJeu.JoueursDehors.RemoveAt(0);
            joueurLocal = Gestionnaire_Joueur.ListGJoueur.Find(j => j.Nom == jLocal.Nom);
            DebutTour();
        }

        public static void BoucleGameplay(List<Joueur> lesJoueurs, Joueur jLocal)
        {
            Init(lesJoueurs,jLocal);

            while (!VerifVictoire().Item1)
            {
                Historique.AjoutLigne("C'est au tour de " + joueurActuel.Nom);
                Historique.AjoutLigne("Bonus dans Tokyo +2 pts victoires pour le joueur à l'intérieur.");

                Interactions.PhaseDes(joueurActuel);
                Interactions.PhaseCartes(joueurActuel);



                Gestionnaire_Des.CleanList();
                
                DebutTour();
            }
            Interactions.Victoire(VerifVictoire().Item2);

        }


        public static void DebutTour()
        {
            EventGame.ResetEventGame();

            tourActuel += 1;
            cptLanceActuel = 0;
            if (Gestionnaire_Joueur.ListGJoueur.Count > PositionTabJoueurActuel +1)
            {
                PositionTabJoueurActuel += 1;
            }
            else
            {
                PositionTabJoueurActuel = 0;
            }
            joueurActuel = Gestionnaire_Joueur.ListGJoueur[PositionTabJoueurActuel];
            if (PlateauDeJeu.EstDedans(joueurActuel))
            {
                joueurActuel.AjouterPtVic(2);
            }
        }
    }

    
}

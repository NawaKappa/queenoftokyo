﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QOT_Domain_GameMaster
{
    public static class Historique
    {
        public static List<string> histo = new List<string>();

        public static void AjoutLigne(string ligne)
        {
            histo.Add(ligne + "\n");
        }

        public static string ReadLine()
        {
            string line = string.Empty;
            lock (histo)
            {
                if (histo.Count > 0)
                {
                    line = histo[0];
                    histo.RemoveAt(0);
                }
            }
            return line;
        }
    }
}

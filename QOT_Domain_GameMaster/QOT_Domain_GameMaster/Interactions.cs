﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using QOT_Domain_Carte;
using QOT_Domain_Joueur;
using QueenOfTokyo_Infrastructure;

namespace QOT_Domain_GameMaster
{
    public static class Interactions
    {
        public static void PhaseDes(Joueur joueurActuel)
        {
            PremierLance(joueurActuel);
            LanceReroll(joueurActuel);
            DesActions(joueurActuel);
            Gestionnaire_Joueur.UpdateGJoueur();
            PlateauDeJeu.UpdatePlateau();
        }

        public static void PhaseCartes(Joueur joueurActuel)
        {
            while (!EventGame.finDeTour)
            {
                if (joueurActuel.Nom == GameMaster.joueurLocal.Nom)
                {
                    while (!EventGame.phaseCarte)
                    { }

                    if (EventGame.finDeTour)
                        Client.SendToServer("FINTOUR");

                    if(EventGame.achatCarte)
                        Client.SendToServer("PHASEACHAT");

                    if (EventGame.venteCarte)
                        Client.SendToServer("PHASEVENTE");
                }
                else
                {
                    Client.SendToServer("OK");
                }


                string receive = Client.ReceiveFromServer();
                if (receive.Contains("FINTOUR"))
                {
                    EventGame.finDeTour = true;
                    Historique.AjoutLigne("Fin du tour.\n");
                    break;
                }

                if (receive.Contains("PHASEACHAT"))
                {
                    AchatCarte(joueurActuel);
                }

                if (receive.Contains("PHASEVENTE"))
                {
                    VenteCarte(joueurActuel);
                }

                EventGame.phaseCarte = false;
                EventGame.achatCarte = false;
                EventGame.venteCarte = false;

            }
            
        }


        public static void Victoire(Joueur jGagnant)
        {
            Historique.AjoutLigne(jGagnant.Nom + " est victorieux !");
        }


        private static void AchatCarte(Joueur joueurActuel)
        {
            if (joueurActuel.Nom == GameMaster.joueurLocal.Nom)
            {

                StringBuilder message = new StringBuilder();
                message.Append("ACHAT:");
                if (EventGame.achatCarte1)
                    message.Append("1");
                if (EventGame.achatCarte2)
                    message.Append("2");
                if (EventGame.achatCarte3)
                    message.Append("3");
                Client.SendToServer(message.ToString());               

            }
            else
            {
                Client.SendToServer("OK");
            }

            string receive = Client.ReceiveFromServer();

            List<Carte> CartesRemove = new List<Carte>();

            if (receive.Contains("1"))
            {
                joueurActuel.EnleverEnergie(Gestionnaire_Cartes.ListeCarteVisibles[0].Prix);
                joueurActuel.ListCartes.Add(Gestionnaire_Cartes.ListeCarteVisibles[0]);
                Historique.AjoutLigne("Achat de la carte " + Gestionnaire_Cartes.ListeCarteVisibles[0].Nom);
                CartesRemove.Add(Gestionnaire_Cartes.ListeCarteVisibles[0]);
            }
            if (receive.Contains("2"))
            {
                joueurActuel.EnleverEnergie(Gestionnaire_Cartes.ListeCarteVisibles[1].Prix);
                joueurActuel.ListCartes.Add(Gestionnaire_Cartes.ListeCarteVisibles[1]);
                Historique.AjoutLigne("Achat de la carte " + Gestionnaire_Cartes.ListeCarteVisibles[1].Nom);
                CartesRemove.Add(Gestionnaire_Cartes.ListeCarteVisibles[1]);
            }
            if (receive.Contains("3"))
            {
                joueurActuel.EnleverEnergie(Gestionnaire_Cartes.ListeCarteVisibles[2].Prix);
                joueurActuel.ListCartes.Add(Gestionnaire_Cartes.ListeCarteVisibles[2]);
                Historique.AjoutLigne("Achat de la carte " + Gestionnaire_Cartes.ListeCarteVisibles[2].Nom);
                CartesRemove.Add(Gestionnaire_Cartes.ListeCarteVisibles[2]);
            }
            Gestionnaire_Cartes.ListeCarteVisibles.RemoveAll(item => CartesRemove.Contains(item));
            EffetInstant(joueurActuel);
        }

        private static void EffetInstant(Joueur joueurActuel)
        {
            List<Carte> CartesRemove = new List<Carte>();
            foreach (Carte carte in joueurActuel.ListCartes)
            {
                if (carte.Nom == "Soin")
                {
                    joueurActuel.AjouterVie(2);
                    Historique.AjoutLigne(carte.Nom + " : soigne 2 points de vie.");
                    CartesRemove.Add(carte);
                }
                if (carte.Nom == "Tramway")
                {
                    joueurActuel.AjouterPtVic(2);
                    Historique.AjoutLigne(carte.Nom + " : ajoute 2 points de victoire.");
                    CartesRemove.Add(carte);
                }
                if (carte.Nom == "Recharge")
                {
                    joueurActuel.AjouterEnergie(9);
                    Historique.AjoutLigne(carte.Nom + " : ajoute 9 points de d'énergie.");
                    CartesRemove.Add(carte);
                }
                if (carte.Nom == "Restauration")
                {
                    joueurActuel.AjouterPtVic(1);
                    Historique.AjoutLigne(carte.Nom + " : ajoute 1 point de victoire.");
                    CartesRemove.Add(carte);
                }
                if (carte.Nom == "Evacuation")
                {
                    foreach (Joueur j in PlateauDeJeu.JoueursDehors)
                    {
                        if (j.Nom != joueurActuel.Nom)
                        {
                            j.EnleverPtVic(5);
                        }
                    }
                    if (PlateauDeJeu.JoueurDedans.Nom != joueurActuel.Nom)
                    {
                        PlateauDeJeu.JoueurDedans.EnleverPtVic(5);
                    }
                    Historique.AjoutLigne(carte.Nom + " : tous les autres joueurs perdent 5 points de victoire.");
                    CartesRemove.Add(carte);
                }
                if (carte.Nom == "Gaz")
                {
                    joueurActuel.AjouterPtVic(2);
                    foreach (Joueur j in PlateauDeJeu.JoueursDehors)
                    {
                        if (j.Nom != joueurActuel.Nom)
                        {
                            j.EnleverVie(3);
                        }
                    }
                    if (PlateauDeJeu.JoueurDedans.Nom != joueurActuel.Nom)
                    {
                        PlateauDeJeu.JoueurDedans.EnleverVie(3);
                    }
                    Historique.AjoutLigne(carte.Nom + " : gagne 2 points de victoire, les autres joueurs perdent 3 points de vie.");
                    CartesRemove.Add(carte);
                }
                if (carte.Nom == "Lanceflamme")
                {
                    foreach (Joueur j in PlateauDeJeu.JoueursDehors)
                    {
                        if (j.Nom != joueurActuel.Nom)
                        {
                            j.EnleverVie(2);
                        }
                    }
                    if (PlateauDeJeu.JoueurDedans.Nom != joueurActuel.Nom)
                    {
                        PlateauDeJeu.JoueurDedans.EnleverVie(2);
                    }
                    CartesRemove.Add(carte);
                    Historique.AjoutLigne(carte.Nom + " : tous les autres joueurs perdent 2 points de vie.");
                }
                
            }
            joueurActuel.ListCartes.RemoveAll(item => CartesRemove.Contains(item));
        }


        private static void VenteCarte(Joueur joueurActuel)
        {
            if (joueurActuel.Nom == GameMaster.joueurLocal.Nom)
            {

                StringBuilder message = new StringBuilder();
                message.Append("VENTE:");
                if (EventGame.venteCarte1)
                    message.Append("1");
                if (EventGame.venteCarte2)
                    message.Append("2");
                if (EventGame.venteCarte3)
                    message.Append("3");
                Client.SendToServer(message.ToString());

            }
            else
            {
                Client.SendToServer("OK");
            }

            string receive = Client.ReceiveFromServer();
            List<Carte> CartesRemove = new List<Carte>();


            if (receive.Contains("1"))
            {
                joueurActuel.AjouterEnergie(joueurActuel.ListCartes[0].Prix - 1);
                Historique.AjoutLigne("Vente de " + joueurActuel.ListCartes[0].Nom + 
                    " : rapporte " + (joueurActuel.ListCartes[0].Prix - 1).ToString() + " points d'énergie.");
                CartesRemove.Add(joueurActuel.ListCartes[0]);
            }
            if (receive.Contains("2"))
            {
                joueurActuel.AjouterEnergie(joueurActuel.ListCartes[1].Prix - 1);
                Historique.AjoutLigne("Vente de " + joueurActuel.ListCartes[1].Nom +
                    " : rapporte " + (joueurActuel.ListCartes[1].Prix - 1).ToString() + " points d'énergie.");
                CartesRemove.Add(joueurActuel.ListCartes[1]);
            }
            if (receive.Contains("3"))
            {
                joueurActuel.AjouterEnergie(joueurActuel.ListCartes[2].Prix - 1);
                Historique.AjoutLigne("Vente de " + joueurActuel.ListCartes[2].Nom +
                    " : rapporte " + (joueurActuel.ListCartes[2].Prix - 1).ToString() + " points d'énergie.");
                CartesRemove.Add(joueurActuel.ListCartes[2]);
            }
            joueurActuel.ListCartes.RemoveAll(item => CartesRemove.Contains(item));

        }



        #region Gestion lancer des dés
        private static void LanceReroll(Joueur joueurActuel)
        {
            while (GameMaster.VerifNbLance())
            {
                Historique.AjoutLigne("Il reste " + (4-GameMaster.cptLanceActuel).ToString() + " lancer à faire.");
                Historique.AjoutLigne("Cocher les dés à changer.");
               /* string input = Console.ReadLine();
                if (input == "y")
                {
                    break;
                }
                else
                {

                }*/

                if (joueurActuel.Nom == GameMaster.joueurLocal.Nom)
                {
                    while (!EventGame.phaseReroll)
                    { }

                    List<Des> desReroll = DesAReroll();
                    joueurActuel.rerollDes(desReroll);

                    StringBuilder message = new StringBuilder();
                    message.Append("REROLL:");
                    message.Append(Gestionnaire_Des.ToStringGDes());

                    Client.SendToServer(message.ToString());
                }
                else
                {
                    Client.SendToServer("OK");
                }

                string listDes = Client.ReceiveFromServer();
                Gestionnaire_Des.StringToGDes(listDes);


                foreach (Des des in Gestionnaire_Des.ListDes)
                {
                    Console.WriteLine(des.Valeur);
                }

             //   if (joueurActuel.Nom == GameMaster.joueurLocal.Nom)
                    EventGame.phaseReroll = false;
            }
           // if (joueurActuel.Nom == GameMaster.joueurLocal.Nom)
                EventGame.phaseDesTerminee = true;
        }

        private static List<Des> DesAReroll()
        {
            List<Des> res = new List<Des>();
            if (EventGame.rerollDes1)
                res.Add(Gestionnaire_Des.ListDes[0]);
            if (EventGame.rerollDes2)
                res.Add(Gestionnaire_Des.ListDes[1]);
            if (EventGame.rerollDes3)
                res.Add(Gestionnaire_Des.ListDes[2]);
            if (EventGame.rerollDes4)
                res.Add(Gestionnaire_Des.ListDes[3]);
            if (EventGame.rerollDes5)
                res.Add(Gestionnaire_Des.ListDes[4]);
            if (EventGame.rerollDes6)
                res.Add(Gestionnaire_Des.ListDes[5]);
            return res;
        }

        private static void PremierLance(Joueur joueurActuel)
        {
            if (GameMaster.VerifNbLance())
            {
                Historique.AjoutLigne("Lancement des dés...");

                //string input = Console.ReadLine();
                if (joueurActuel.Nom == GameMaster.joueurLocal.Nom)
                {
                    while (!EventGame.premierLance)
                    { }
                    joueurActuel.lancerDes();
                    StringBuilder message = new StringBuilder();
                    message.Append("LISTDES:");
                    message.Append(Gestionnaire_Des.ToStringGDes());

                    Client.SendToServer(message.ToString());
                }
                else
                {
                    Client.SendToServer("OK");
                }



                string listDes = Client.ReceiveFromServer();
                Gestionnaire_Des.StringToGDes(listDes);

                //foreach (Des des in Gestionnaire_Des.ListDes)
                //{
                //    Console.WriteLine(des.Valeur);
                //}
                EventGame.affichageLance = true;
               // EventGame.premierLance = false;
            }
        }
        #endregion

        private static void DesActions(Joueur jActuel)
        {
            GestionDesPtVictoires(jActuel);
            GestionDesEnergies(jActuel);
            GestionDesCoeurs(jActuel);
            GestionDesBaffes(jActuel);
        }

        #region Gestion points de victoires sur les dés
        private static void GestionDesPtVictoires(Joueur jActuel)
        {
            int occurencesUn = Gestionnaire_Des.ListDes.Count(ServicePredicate.PredicateDesPtVictoire_Un);
            int occurencesDeux = Gestionnaire_Des.ListDes.Count(ServicePredicate.PredicateDesPtVictoire_Deux);
            int occurencesTrois = Gestionnaire_Des.ListDes.Count(ServicePredicate.PredicateDesPtVictoire_Trois);

            if (occurencesUn >= 3)
            {
                GestionDesAjoutPtVictoire(jActuel, occurencesUn, 1);
            }
            if (occurencesDeux >= 3)
            {
                GestionDesAjoutPtVictoire(jActuel, occurencesDeux, 2);
            }
            if (occurencesTrois >= 3)
            {
                GestionDesAjoutPtVictoire(jActuel, occurencesTrois, 3);
            }
        }

        private static void GestionDesAjoutPtVictoire(Joueur jActuel, int occurences, int ptAjoute)
        {
            int pt = ptAjoute;
            jActuel.AjouterPtVic(ptAjoute);
            occurences -= 3;
            for (int i = 0; i < occurences; i++)
            {
                jActuel.AjouterPtVic(ptAjoute);
                pt += ptAjoute;
            }
            Historique.AjoutLigne("Gagne " + ptAjoute + " points de victoires");

        }
        #endregion

        private static void GestionDesEnergies(Joueur jActuel)
        {
            int nbCartesAmi = jActuel.ListCartes.Count(c => c.Nom == "AmiEnfants");
            if(Gestionnaire_Des.ListDes.Count(ServicePredicate.PredicateDesPtEnergie) * (nbCartesAmi + 1) > 0)
                Historique.AjoutLigne("Gagne " + Gestionnaire_Des.ListDes.Count(ServicePredicate.PredicateDesPtEnergie) * (nbCartesAmi + 1) +" points d'énergie");
            jActuel.AjouterEnergie(Gestionnaire_Des.ListDes.Count(ServicePredicate.PredicateDesPtEnergie)*(nbCartesAmi+1));
        }

        private static void GestionDesCoeurs(Joueur jActuel)
        {
            if (PlateauDeJeu.EstDehors(jActuel))
            {
                int nbRegen = jActuel.ListCartes.Count(c => c.Nom == "Regeneration") + 1;
                Historique.AjoutLigne("Gagne " + Gestionnaire_Des.ListDes.Count(ServicePredicate.PredicateDesCoeur) * nbRegen + " points de vie.");
                jActuel.AjouterVie(Gestionnaire_Des.ListDes.Count(ServicePredicate.PredicateDesCoeur)*nbRegen);
            }
        }

        private static void GestionDesBaffes(Joueur jActuel)
        {
            int nbAcideCarte = jActuel.ListCartes.Count(c => c.Nom == "Acide");
            int nbBaffes = Gestionnaire_Des.ListDes.Count(ServicePredicate.PredicateDesBaffes) + nbAcideCarte;

            if (nbBaffes > 0)
            {
                if (PlateauDeJeu.EstDedans(jActuel))
                {
                    foreach (Joueur joueur in PlateauDeJeu.JoueursDehors)
                    {
                        if (joueur != jActuel)
                        {
                            joueur.EnleverVie(nbBaffes);
                        }
                    }
                    Historique.AjoutLigne("Inflige " + nbBaffes+" points de dégats aux autres joueurs.");
                }
                else if (PlateauDeJeu.EstDehors(jActuel))
                {
                    PlateauDeJeu.JoueurDedans.EnleverVie(nbBaffes);
                    Historique.AjoutLigne("Inflige " + nbBaffes + " points de dégats au joueur dans Tokyo.");
                    JoueurDedansEstBaffe(jActuel);

                }
            }
        }

        private static void JoueurDedansEstBaffe(Joueur jActuel)
        {
            Console.WriteLine("{0} vous a baffé, voulez-vous échanger de place ?", jActuel.Nom);
            EventGame.blockFinDeTour = true;
            if (GameMaster.joueurLocal.Nom == PlateauDeJeu.JoueurDedans.Nom)
            {
                EventGame.argJeSuisTouche = true;
                string message;

                while (!EventGame.reponseSortir) { }

                if (EventGame.jeVeuxSortir)
                {
                    message = "JESORS";
                }
                else
                {
                    message = "JERESTE";
                }
                Client.SendToServer(message);
            }
            else
            {
                Client.SendToServer("OK");
            }

            //string input = Console.ReadLine();
            //if (input == "y")
            //{
            //    PlateauDeJeu.Echange(jActuel);
            //}
            string reception = Client.ReceiveFromServer();
            if (reception == "swap")
            {
                PlateauDeJeu.Echange(jActuel);
                Historique.AjoutLigne("Le joueur à l'intérieur décide d'échanger sa place.");

            }
            else
            {
                Historique.AjoutLigne("Le joueur à l'intérieur décide de ne pas échanger sa place.");
            }

            EventGame.reponseSortir = false;
            EventGame.blockFinDeTour = false;
        }


    }
}

﻿using QOT_Domain_Joueur;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QOT_Domain_GameMaster
{
    class Program
    {
        static void Main(string[] args)
        {
            Joueur player = new Joueur("Gilles");
            Joueur player2 = new Joueur("Lucas");
            Joueur player3 = new Joueur("Jean");
            Joueur player4 = new Joueur("Guy");
            List<Joueur>listJoueurs = new List<Joueur>(){ player, player2, player3, player4};

           // GameMaster.BoucleGameplay(listJoueurs);


            Console.ReadLine();
        }
    }
}

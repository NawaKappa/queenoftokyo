﻿using QOT_Domain_Joueur;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QOT_Domain_GameMaster
{
    public static class ServicePredicate
    {
        public static bool PredicateDesPtVictoire_Un(Des des)
        {
            if (des.Valeur == "Un")
                return true;
            else
                return false;
        }

        public static bool PredicateDesPtVictoire_Deux(Des des)
        {
            if (des.Valeur == "Deux")
                return true;
            else
                return false;
        }

        public static bool PredicateDesPtVictoire_Trois(Des des)
        {
            if (des.Valeur == "Trois")
                return true;
            else
                return false;
        }

        public static bool PredicateDesPtEnergie(Des des)
        {
            if (des.Valeur == "Energie")
                return true;
            else
                return false;
        }

        public static bool PredicateDesCoeur(Des des)
        {
            if (des.Valeur == "Coeur")
                return true;
            else
                return false;
        }

        public static bool PredicateDesBaffes(Des des)
        {
            if (des.Valeur == "Baffe")
                return true;
            else
                return false;
        }

    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;

namespace QOT_Domain_Joueur
{
    class ClassTest
    {
        static void Main(string[] args)
        {
            #region CreaJoueur
            Joueur player = new Joueur("alfred");
            Joueur player2 = new Joueur("aled");
            Joueur player3 = new Joueur("ocecour");
            #endregion

            #region Test LancerDes
            /* player.lancerDes();

             foreach (Des des in Gestionnaire_Des.ListDes)
             {
                 Console.WriteLine(des.Id + " " + des.Valeur);
             }

             List<Des> listReroll = new List<Des>();
             listReroll.Add(Gestionnaire_Des.ListDes[2]);
             listReroll.Add(Gestionnaire_Des.ListDes[4]);
             player.rerollDes(listReroll);

             Console.WriteLine("\n");
             foreach (Des des in Gestionnaire_Des.ListDes)
             {
                 Console.WriteLine(des.Id + " " + des.Valeur);
             }*/
            #endregion

            #region Test Add/Supp
            /*  Console.WriteLine(player.NbEnergie);
              Console.WriteLine(player.AjouterEnergie(6));
              Console.WriteLine(player.AjouterEnergie(2));
              */

            //Console.WriteLine(player.NbEnergie);
            //Console.WriteLine(player.AjouterEnergie(6));
            //Console.WriteLine(player.EnleverEnergie(2));
            //Console.WriteLine(player.EnleverEnergie(26));

            /*Console.WriteLine(player.ParametreJoueur.nbLance);
            player.ParametreJoueur.nbLance += 1;
            Console.WriteLine(player.ParametreJoueur.nbLance);*/
            #endregion

            #region Test Plateau
            PlateauDeJeu.JoueursDehors.Add(player);
            PlateauDeJeu.JoueurDedans =player2;
            PlateauDeJeu.JoueursDehors.Add(player3);

            Console.WriteLine("dehors: ");
            foreach (Joueur leJoueur in PlateauDeJeu.JoueursDehors)
            {
                Console.WriteLine(leJoueur.Nom);
            }
            Console.WriteLine("\ndedans: ");
            Console.WriteLine(PlateauDeJeu.JoueurDedans.Nom);

            PlateauDeJeu.Echange(player);
            Console.WriteLine("\n\ndehors: ");
            foreach (Joueur leJoueur in PlateauDeJeu.JoueursDehors)
            {
                Console.WriteLine(leJoueur.Nom);
            }
            Console.WriteLine("\ndedans: ");
            Console.WriteLine(PlateauDeJeu.JoueurDedans.Nom);

            Console.WriteLine(PlateauDeJeu.EstDedans(player));
            Console.WriteLine(PlateauDeJeu.EstDedans(player2));
            Console.WriteLine(PlateauDeJeu.EstDedans(player3));
            Console.WriteLine(PlateauDeJeu.EstDehors(player2));
            #endregion

            Console.ReadLine();
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QOT_Domain_Joueur
{
    public class Des
    {
        public string Valeur { get; set; }
        public int Id { get; set; }
        private static Random Rdm = new Random();

       

        public Des(string valeur, int id)
        {
            Valeur = valeur;
            Id = id;
        }

        public Des()
        {
            Valeur = ValeurAleatoire();
            Id = -1;
        }

        public Des(int id)
        {
            Valeur = ValeurAleatoire();
            Id = id;
        }

        public static string ValeurAleatoire()
        {
            int valueToChoose = ChoisirValeurDansTableau();
            Array values = Enum.GetValues(typeof(EnumValeurDes));
            EnumValeurDes randomVal = (EnumValeurDes)values.GetValue(valueToChoose);
            return randomVal.ToString();
        }

        private static int ChoisirValeurDansTableau()
        {
           
            int valueToChoose = Rdm.Next(0, 6);
            return valueToChoose;
        }
    }
}

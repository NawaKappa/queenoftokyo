﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QOT_Domain_Joueur
{
    public static class Gestionnaire_Des
    {
        public static List<Des> ListDes = new List<Des>();
        private static int LastId = -1;

        public static void SupprimerDes(Des des)
        {
            if (ListDes.Contains(des))
            {
                ListDes.Remove(des);
                Console.WriteLine("Le dé a été supprimé.");
            }
            else
            {
                Console.WriteLine("Le dé n'existe pas dans le gestionnaire");
            }

            
        }

        public static void SupprimerDesParId(int id)
        {
            bool found = false;

            for (int i = 0; i <= ListDes.Count; i++)
            {
                if (ListDes[i].Id == id)
                {
                    ListDes.RemoveAt(i);
                    found = true;
                    Console.WriteLine("Le dé a été supprimé.");
                    
                    break;
                }
            }

            if (!found)
            {
                Console.WriteLine("Le dé n'existe pas dans le gestionnaire");
            }

        }

        public static Des AjouterNouveauDes()
        {
            Des des = new Des(LastId + 1);
            LastId++;
            ListDes.Add(des);

            return des;
        }

        public static void CleanList()
        {
            ListDes.RemoveRange(0, ListDes.Count);
            LastId = -1;
        }

        public static void ChangerValeurDes(int id)
        {
            int index = ListDes.FindIndex(x => x.Id == id);
            ListDes[index].Valeur = Des.ValeurAleatoire();
        }

        public static string ToStringGDes()
        {
            StringBuilder str = new StringBuilder();
            foreach (Des des in Gestionnaire_Des.ListDes)
            {
                str.Append(des.Valeur + ";");
            }
            str.Remove(str.Length - 1, 1);

            return str.ToString();
        }

        public static void StringToGDes(string listDes)
        {
            List<string> tmp = listDes.Split(';').ToList();
            for (int i = 0; i <tmp.Count; i++)
            {
                Des tmpDes = new Des();
                tmpDes.Valeur = tmp[i];
                tmpDes.Id = i;
                if (Gestionnaire_Des.ListDes.Count < 6)
                {
                    Gestionnaire_Des.ListDes.Add(tmpDes);
                }
                else
                {
                    Gestionnaire_Des.ListDes[i] = tmpDes;
                }
            }

        }


    }
}

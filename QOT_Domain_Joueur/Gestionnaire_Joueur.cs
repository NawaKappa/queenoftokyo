﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QOT_Domain_Joueur
{
    public static class Gestionnaire_Joueur
    {
        public static List<Joueur> ListGJoueur = new List<Joueur>();



        public static void AjouterJoueur(Joueur unJoueur)
        {
            if (ListGJoueur.Contains(unJoueur)) 
            {
                throw new ArgumentException("Le joueur existe deja");
            }
            else
            {
                ListGJoueur.Add(unJoueur);
            }

        }

        public static void SupprimerJoueur(Joueur unJoueur)
        {
            if (!ListGJoueur.Contains(unJoueur))
            {
                throw new ArgumentException("Le joueur n'existe pas");
            }
            else
            {
                ListGJoueur.Remove(unJoueur);
            }
        }

        public static void UpdateGJoueur()
        {
            ListGJoueur.RemoveAll(j => j.estMort());
        }

    }
}

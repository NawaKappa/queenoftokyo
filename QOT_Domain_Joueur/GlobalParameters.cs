﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QOT_Domain_Joueur
{
    public class GlobalParameters
    {
        public int nbLance = 3;
        public int nbDes = 6;
        public int nbMaxCarte = 3;
        public int nbMaxVie = 10;

        public GlobalParameters()
        {
        }
    }
}

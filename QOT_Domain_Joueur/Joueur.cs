﻿using QOT_Domain_Carte;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QOT_Domain_Joueur
{
    /// <summary>
    /// Classe du joueur
    /// </summary>
    public class Joueur
    {
        #region Params
        public string Nom { get;  set; }
        public int Id { get;  set; }
        public int NbEnergie { get; set; }
        public List<Carte> ListCartes { get; set; }
        public int Vie { get; set; }
        public int PtVictoire { get; set; }
        public string NomAvatar { get;  set; }
        public GlobalParameters ParametreJoueur { get; set; }
        public int DesDebut { get; set; }
        #endregion

        #region Constructeur
        public Joueur(string nom, int id, int nbEnergie, List<Carte> listCartes, int vie, int ptVictoire, string nomAvatar, GlobalParameters parametreJoueur)
        {
            Nom = nom;
            Id = id;
            NbEnergie = nbEnergie;
            ListCartes = listCartes;
            Vie = vie;
            PtVictoire = ptVictoire;
            NomAvatar = nomAvatar;
            ParametreJoueur = parametreJoueur;
        }

        public Joueur()
        {
            Nom = "NomJoueur";
            Id = 0;
            NbEnergie = 0;
            ListCartes = new List<Carte>();
            Vie = 10;
            PtVictoire = 0;
            NomAvatar = "";

            GlobalParameters para = new GlobalParameters();
            ParametreJoueur = para;
        }

        public Joueur(string nom)
        {
            Nom = nom;
            Id = 0;
            NbEnergie = 0;
            ListCartes = new List<Carte>();
            Vie = 10;
            PtVictoire = 0;
            NomAvatar = "Choix en cours...";

            GlobalParameters para = new GlobalParameters();
            ParametreJoueur = para;
        }
        #endregion

        #region OptionDes
        public void lancerDes()
        {
            Gestionnaire_Des.CleanList();
            for (int i = 0; i < ParametreJoueur.nbDes; i++)
            {
                Gestionnaire_Des.AjouterNouveauDes();
            }
        }

        public void rerollDes(List<Des> desAReroll)
        {
            foreach (Des desReroll in desAReroll)
            {
                Gestionnaire_Des.ChangerValeurDes(desReroll.Id);               
            }
        }
        #endregion

        #region Ajouter-Enlever(energie,vie,ptVic)
        public int AjouterEnergie(int energie)
        {
            this.NbEnergie += energie;
            return this.NbEnergie;
        }

        public int EnleverEnergie(int energie)
        {
            if (this.NbEnergie< energie)
            {
                this.NbEnergie = 0;
                return this.NbEnergie;
            }
            else
            {
                this.NbEnergie -= energie;
                return this.NbEnergie;
            }
          
        }

        public int AjouterVie(int vie)
        {
            if (this.Vie + vie > 10)
                this.Vie = 10;
            else
                this.Vie += vie;
            return this.Vie;
        }

        public int EnleverVie(int vie)
        {
            if (this.Vie < vie)
            {
                this.Vie = 0;
                return this.Vie;
            }
            else
            {
                this.Vie -= vie;
                return this.Vie;
            }

        }

        public int AjouterPtVic(int ptVic)
        {
            this.PtVictoire += ptVic;
            return this.PtVictoire;
        }

        public int EnleverPtVic(int ptVic)
        {
            if (this.PtVictoire < ptVic)
            {
                this.PtVictoire = 0;
                return this.PtVictoire;
            }
            else
            {
                this.PtVictoire -= ptVic;
                return this.PtVictoire;
            }

        }
        #endregion

        #region Cartes

        #endregion

        public bool estMort()
        {
            if(this.Vie <= 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public Joueur Copy()
        {
            var result = new Joueur();
            result.Id = this.Id;
            result.ListCartes = this.ListCartes;
            result.NbEnergie = this.NbEnergie;
            result.Id = this.Id;
            result.Id = this.Id;
            return result;
        }

        override public string ToString()
        {
            StringBuilder strB = new StringBuilder();
            strB.Append(this.Nom + ";" + this.NomAvatar + ";" + this.NbEnergie +
                        ";" + this.Vie + ";" + this.PtVictoire + ";" + this.DesDebut);

            return strB.ToString();
        }

        public Joueur StringToJoueur(string joueur)
        {
            Joueur res = new Joueur();
            List<string> listParam = joueur.Split(';').ToList();

            res.Nom = listParam[0];
            res.NomAvatar = listParam[1];
            res.NbEnergie = int.Parse(listParam[2]);
            res.Vie = int.Parse(listParam[3]);
            res.PtVictoire = int.Parse(listParam[4]);
            res.DesDebut = int.Parse(listParam[5]);

            return res;
        }


    }
}

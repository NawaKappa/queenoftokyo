﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QOT_Domain_Joueur
{

    public static class PlateauDeJeu
    {
        public static List<Joueur> JoueursDehors = new List<Joueur>();
        public static Joueur JoueurDedans = null;

        public static void Echange(Joueur leJoueur)
        {
            JoueursDehors.Add(JoueurDedans);
            leJoueur.AjouterPtVic(1);
            JoueurDedans = leJoueur;
            JoueursDehors.Remove(leJoueur);
        }

        public static bool EstDedans(Joueur joueur)
        {
            if (JoueurDedans == joueur)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public static bool EstDehors(Joueur joueur)
        {
            return JoueursDehors.Contains(joueur);
        }

        public static void UpdatePlateau()
        {
            JoueursDehors.RemoveAll(j => j.estMort());
            if (JoueurDedans.estMort())
            {
                JoueurDedans = JoueursDehors[0];
                JoueursDehors.RemoveAt(0);
            }
        }

    }

   


}

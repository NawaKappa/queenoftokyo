﻿namespace QOT_UserInterface
{
    partial class Accueil
    {
        /// <summary>
        /// Variable nécessaire au concepteur.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Nettoyage des ressources utilisées.
        /// </summary>
        /// <param name="disposing">true si les ressources managées doivent être supprimées ; sinon, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Code généré par le Concepteur Windows Form

        /// <summary>
        /// Méthode requise pour la prise en charge du concepteur - ne modifiez pas
        /// le contenu de cette méthode avec l'éditeur de code.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblPseudo = new System.Windows.Forms.Label();
            this.txtPseudo = new System.Windows.Forms.TextBox();
            this.btnCreerPartie = new System.Windows.Forms.Button();
            this.btnRejoindrePartie = new System.Windows.Forms.Button();
            this.btnQuitter = new System.Windows.Forms.Button();
            this.pictBLogo = new System.Windows.Forms.PictureBox();
            this.button2 = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.pictBLogo)).BeginInit();
            this.SuspendLayout();
            // 
            // lblPseudo
            // 
            this.lblPseudo.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.lblPseudo.AutoSize = true;
            this.lblPseudo.Location = new System.Drawing.Point(620, 268);
            this.lblPseudo.Name = "lblPseudo";
            this.lblPseudo.Size = new System.Drawing.Size(43, 13);
            this.lblPseudo.TabIndex = 1;
            this.lblPseudo.Text = "Pseudo";
            // 
            // txtPseudo
            // 
            this.txtPseudo.AccessibleName = "";
            this.txtPseudo.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.txtPseudo.Location = new System.Drawing.Point(669, 265);
            this.txtPseudo.Name = "txtPseudo";
            this.txtPseudo.Size = new System.Drawing.Size(142, 20);
            this.txtPseudo.TabIndex = 2;
            this.txtPseudo.TextChanged += new System.EventHandler(this.txtPseudo_TextChanged);
            // 
            // btnCreerPartie
            // 
            this.btnCreerPartie.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.btnCreerPartie.Location = new System.Drawing.Point(655, 336);
            this.btnCreerPartie.Name = "btnCreerPartie";
            this.btnCreerPartie.Size = new System.Drawing.Size(174, 40);
            this.btnCreerPartie.TabIndex = 3;
            this.btnCreerPartie.Text = "Créer une partie";
            this.btnCreerPartie.UseVisualStyleBackColor = true;
            this.btnCreerPartie.Click += new System.EventHandler(this.btnCreerPartie_Click);
            // 
            // btnRejoindrePartie
            // 
            this.btnRejoindrePartie.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.btnRejoindrePartie.Location = new System.Drawing.Point(655, 406);
            this.btnRejoindrePartie.Name = "btnRejoindrePartie";
            this.btnRejoindrePartie.Size = new System.Drawing.Size(174, 48);
            this.btnRejoindrePartie.TabIndex = 4;
            this.btnRejoindrePartie.Text = "Rejoindre une partie";
            this.btnRejoindrePartie.UseVisualStyleBackColor = true;
            this.btnRejoindrePartie.Click += new System.EventHandler(this.btnRejoindrePartie_Click);
            // 
            // btnQuitter
            // 
            this.btnQuitter.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnQuitter.Location = new System.Drawing.Point(990, 523);
            this.btnQuitter.Name = "btnQuitter";
            this.btnQuitter.Size = new System.Drawing.Size(75, 23);
            this.btnQuitter.TabIndex = 5;
            this.btnQuitter.Text = "Quitter";
            this.btnQuitter.UseVisualStyleBackColor = true;
            this.btnQuitter.Click += new System.EventHandler(this.btnQuitter_Click);
            // 
            // pictBLogo
            // 
            this.pictBLogo.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.pictBLogo.Image = global::QOT_UserInterface.Properties.Resources.logo;
            this.pictBLogo.Location = new System.Drawing.Point(395, 22);
            this.pictBLogo.Name = "pictBLogo";
            this.pictBLogo.Size = new System.Drawing.Size(656, 182);
            this.pictBLogo.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.pictBLogo.TabIndex = 0;
            this.pictBLogo.TabStop = false;
            // 
            // button2
            // 
            this.button2.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.button2.Location = new System.Drawing.Point(655, 476);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(174, 48);
            this.button2.TabIndex = 7;
            this.button2.Text = "Règles";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // Accueil
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(1420, 612);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.btnQuitter);
            this.Controls.Add(this.btnRejoindrePartie);
            this.Controls.Add(this.btnCreerPartie);
            this.Controls.Add(this.txtPseudo);
            this.Controls.Add(this.lblPseudo);
            this.Controls.Add(this.pictBLogo);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.Name = "Accueil";
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
            this.Text = "Accueil";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.Accueil_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pictBLogo)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox pictBLogo;
        private System.Windows.Forms.Label lblPseudo;
        private System.Windows.Forms.Button btnCreerPartie;
        private System.Windows.Forms.Button btnRejoindrePartie;
        private System.Windows.Forms.Button btnQuitter;
        public System.Windows.Forms.TextBox txtPseudo;
        private System.Windows.Forms.Button button2;
    }
}


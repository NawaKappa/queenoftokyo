﻿using QueenOfTokyo_Infrastructure;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace QOT_UserInterface
{
    public partial class Accueil : Form
    {
        public Accueil()
        {
            InitializeComponent();
        }


        private bool PseudoEstVide()
        {
            if (txtPseudo.Text.Equals(string.Empty))
            {
                MessageBox.Show("Veuillez mettre un pseudo.");
                return true;
            }
            return false;
        }

        private void btnQuitter_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void btnCreerPartie_Click(object sender, EventArgs e)
        {
            if (!PseudoEstVide())
            {
                CreationLobby creaLobby = new CreationLobby(this);
                this.Visible = false;
                creaLobby.ShowDialog();
            }
        }


        private void txtPseudo_TextChanged(object sender, EventArgs e)
        {
        }

        private void Accueil_Load(object sender, EventArgs e)
        {
           
        }

        private void btnRejoindrePartie_Click(object sender, EventArgs e)
        {
            if (!PseudoEstVide())
            {
                RejoindreLobby RejLobby = new RejoindreLobby(this);
                this.Visible = false;
                RejLobby.ShowDialog();
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            string locationToSavePdf = Path.Combine(Path.GetTempPath(), "QUEEN_OF_TOKYO  regle.pdf");  // select other location if you want
            File.WriteAllBytes(locationToSavePdf, Properties.Resources.QUEEN_OF_TOKYO__regle);    // write the file from the resources to the location you want
            Process.Start(locationToSavePdf);
        }
    }
}

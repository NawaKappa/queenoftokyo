﻿namespace QOT_UserInterface
{
    partial class ChoixPerso
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label98 = new System.Windows.Forms.Label();
            this.IPServer = new System.Windows.Forms.Label();
            this.Pseudo1 = new System.Windows.Forms.Label();
            this.Pseudo2 = new System.Windows.Forms.Label();
            this.Pseudo3 = new System.Windows.Forms.Label();
            this.Pseudo4 = new System.Windows.Forms.Label();
            this.Avatar1 = new System.Windows.Forms.Label();
            this.Avatar3 = new System.Windows.Forms.Label();
            this.Avatar2 = new System.Windows.Forms.Label();
            this.Avatar4 = new System.Windows.Forms.Label();
            this.Valider = new System.Windows.Forms.Button();
            this.pictureBoxDroite = new System.Windows.Forms.PictureBox();
            this.pictureBoxGauche = new System.Windows.Forms.PictureBox();
            this.pictBoxAvatar = new System.Windows.Forms.PictureBox();
            this.pictBLogo = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxDroite)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxGauche)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictBoxAvatar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictBLogo)).BeginInit();
            this.SuspendLayout();
            // 
            // label98
            // 
            this.label98.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.label98.AutoSize = true;
            this.label98.Location = new System.Drawing.Point(44, 66);
            this.label98.Name = "label98";
            this.label98.Size = new System.Drawing.Size(66, 13);
            this.label98.TabIndex = 7;
            this.label98.Text = "IP Serveur : ";
            this.label98.Click += new System.EventHandler(this.label87_Click);
            // 
            // IPServer
            // 
            this.IPServer.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.IPServer.AutoSize = true;
            this.IPServer.Location = new System.Drawing.Point(116, 66);
            this.IPServer.Name = "IPServer";
            this.IPServer.Size = new System.Drawing.Size(23, 13);
            this.IPServer.TabIndex = 8;
            this.IPServer.Text = "(IP)";
            // 
            // Pseudo1
            // 
            this.Pseudo1.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.Pseudo1.AutoSize = true;
            this.Pseudo1.Location = new System.Drawing.Point(372, 331);
            this.Pseudo1.Name = "Pseudo1";
            this.Pseudo1.Size = new System.Drawing.Size(49, 13);
            this.Pseudo1.TabIndex = 7;
            this.Pseudo1.Text = "Pseudo1";
            this.Pseudo1.Click += new System.EventHandler(this.label87_Click);
            // 
            // Pseudo2
            // 
            this.Pseudo2.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.Pseudo2.AutoSize = true;
            this.Pseudo2.Location = new System.Drawing.Point(372, 359);
            this.Pseudo2.Name = "Pseudo2";
            this.Pseudo2.Size = new System.Drawing.Size(65, 13);
            this.Pseudo2.TabIndex = 7;
            this.Pseudo2.Text = "En attente...";
            this.Pseudo2.Visible = false;
            this.Pseudo2.Click += new System.EventHandler(this.label87_Click);
            // 
            // Pseudo3
            // 
            this.Pseudo3.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.Pseudo3.AutoSize = true;
            this.Pseudo3.Location = new System.Drawing.Point(372, 390);
            this.Pseudo3.Name = "Pseudo3";
            this.Pseudo3.Size = new System.Drawing.Size(65, 13);
            this.Pseudo3.TabIndex = 7;
            this.Pseudo3.Text = "En attente...";
            this.Pseudo3.Visible = false;
            this.Pseudo3.Click += new System.EventHandler(this.label87_Click);
            // 
            // Pseudo4
            // 
            this.Pseudo4.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.Pseudo4.AutoSize = true;
            this.Pseudo4.Location = new System.Drawing.Point(372, 419);
            this.Pseudo4.Name = "Pseudo4";
            this.Pseudo4.Size = new System.Drawing.Size(65, 13);
            this.Pseudo4.TabIndex = 7;
            this.Pseudo4.Text = "En attente...";
            this.Pseudo4.Visible = false;
            this.Pseudo4.Click += new System.EventHandler(this.label87_Click);
            // 
            // Avatar1
            // 
            this.Avatar1.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.Avatar1.AutoSize = true;
            this.Avatar1.Location = new System.Drawing.Point(444, 331);
            this.Avatar1.Name = "Avatar1";
            this.Avatar1.Size = new System.Drawing.Size(44, 13);
            this.Avatar1.TabIndex = 7;
            this.Avatar1.Text = "Avatar1";
            this.Avatar1.Visible = false;
            this.Avatar1.Click += new System.EventHandler(this.label87_Click);
            // 
            // Avatar3
            // 
            this.Avatar3.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.Avatar3.AutoSize = true;
            this.Avatar3.Location = new System.Drawing.Point(444, 390);
            this.Avatar3.Name = "Avatar3";
            this.Avatar3.Size = new System.Drawing.Size(44, 13);
            this.Avatar3.TabIndex = 7;
            this.Avatar3.Text = "Avatar3";
            this.Avatar3.Visible = false;
            this.Avatar3.Click += new System.EventHandler(this.label87_Click);
            // 
            // Avatar2
            // 
            this.Avatar2.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.Avatar2.AutoSize = true;
            this.Avatar2.Location = new System.Drawing.Point(444, 359);
            this.Avatar2.Name = "Avatar2";
            this.Avatar2.Size = new System.Drawing.Size(44, 13);
            this.Avatar2.TabIndex = 7;
            this.Avatar2.Text = "Avatar2";
            this.Avatar2.Visible = false;
            this.Avatar2.Click += new System.EventHandler(this.label87_Click);
            // 
            // Avatar4
            // 
            this.Avatar4.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.Avatar4.AutoSize = true;
            this.Avatar4.Location = new System.Drawing.Point(444, 419);
            this.Avatar4.Name = "Avatar4";
            this.Avatar4.Size = new System.Drawing.Size(44, 13);
            this.Avatar4.TabIndex = 7;
            this.Avatar4.Text = "Avatar4";
            this.Avatar4.Visible = false;
            this.Avatar4.Click += new System.EventHandler(this.label87_Click);
            // 
            // Valider
            // 
            this.Valider.Location = new System.Drawing.Point(833, 558);
            this.Valider.Name = "Valider";
            this.Valider.Size = new System.Drawing.Size(142, 43);
            this.Valider.TabIndex = 10;
            this.Valider.Text = "Valider";
            this.Valider.UseVisualStyleBackColor = true;
            this.Valider.Click += new System.EventHandler(this.Valider_Click);
            // 
            // pictureBoxDroite
            // 
            this.pictureBoxDroite.Image = global::QOT_UserInterface.Properties.Resources.Fleche_droite;
            this.pictureBoxDroite.Location = new System.Drawing.Point(1004, 359);
            this.pictureBoxDroite.Name = "pictureBoxDroite";
            this.pictureBoxDroite.Size = new System.Drawing.Size(65, 67);
            this.pictureBoxDroite.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBoxDroite.TabIndex = 12;
            this.pictureBoxDroite.TabStop = false;
            this.pictureBoxDroite.Click += new System.EventHandler(this.pictureBox2_Click);
            // 
            // pictureBoxGauche
            // 
            this.pictureBoxGauche.Image = global::QOT_UserInterface.Properties.Resources.Fleche_gauche;
            this.pictureBoxGauche.Location = new System.Drawing.Point(731, 359);
            this.pictureBoxGauche.Name = "pictureBoxGauche";
            this.pictureBoxGauche.Size = new System.Drawing.Size(65, 67);
            this.pictureBoxGauche.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBoxGauche.TabIndex = 11;
            this.pictureBoxGauche.TabStop = false;
            this.pictureBoxGauche.Click += new System.EventHandler(this.pictureBox1_Click);
            // 
            // pictBoxAvatar
            // 
            this.pictBoxAvatar.Location = new System.Drawing.Point(802, 253);
            this.pictBoxAvatar.Name = "pictBoxAvatar";
            this.pictBoxAvatar.Size = new System.Drawing.Size(196, 287);
            this.pictBoxAvatar.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictBoxAvatar.TabIndex = 9;
            this.pictBoxAvatar.TabStop = false;
            this.pictBoxAvatar.Click += new System.EventHandler(this.pictBoxAvatar_Click);
            // 
            // pictBLogo
            // 
            this.pictBLogo.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.pictBLogo.Image = global::QOT_UserInterface.Properties.Resources.logo;
            this.pictBLogo.Location = new System.Drawing.Point(404, 48);
            this.pictBLogo.Name = "pictBLogo";
            this.pictBLogo.Size = new System.Drawing.Size(656, 182);
            this.pictBLogo.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.pictBLogo.TabIndex = 2;
            this.pictBLogo.TabStop = false;
            // 
            // ChoixPerso
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.ClientSize = new System.Drawing.Size(1387, 640);
            this.Controls.Add(this.pictureBoxDroite);
            this.Controls.Add(this.pictureBoxGauche);
            this.Controls.Add(this.Valider);
            this.Controls.Add(this.pictBoxAvatar);
            this.Controls.Add(this.IPServer);
            this.Controls.Add(this.Pseudo4);
            this.Controls.Add(this.Pseudo3);
            this.Controls.Add(this.Avatar2);
            this.Controls.Add(this.Pseudo2);
            this.Controls.Add(this.Avatar4);
            this.Controls.Add(this.Avatar3);
            this.Controls.Add(this.Avatar1);
            this.Controls.Add(this.Pseudo1);
            this.Controls.Add(this.label98);
            this.Controls.Add(this.pictBLogo);
            this.Name = "ChoixPerso";
            this.Text = "ChoixPerso";
            this.Load += new System.EventHandler(this.ChoixPerso_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxDroite)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxGauche)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictBoxAvatar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictBLogo)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox pictBLogo;
        private System.Windows.Forms.Label label98;
        private System.Windows.Forms.Label IPServer;
        private System.Windows.Forms.Label Pseudo1;
        private System.Windows.Forms.Label Pseudo2;
        private System.Windows.Forms.Label Pseudo3;
        private System.Windows.Forms.Label Pseudo4;
        private System.Windows.Forms.Label Avatar1;
        private System.Windows.Forms.Label Avatar3;
        private System.Windows.Forms.Label Avatar2;
        private System.Windows.Forms.Label Avatar4;
        private System.Windows.Forms.PictureBox pictBoxAvatar;
        private System.Windows.Forms.Button Valider;
        private System.Windows.Forms.PictureBox pictureBoxGauche;
        private System.Windows.Forms.PictureBox pictureBoxDroite;
    }
}
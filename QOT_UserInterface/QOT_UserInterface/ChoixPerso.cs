﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using QOT_Application;
using QOT_Domain_Joueur;
using QueenOfTokyo_Infrastructure;

namespace QOT_UserInterface
{
    public partial class ChoixPerso : Form
    {
        private Thread tServer;
        private Task tAjoutNomAv;

        private CreationLobby _Crea;
        private RejoindreLobby _Join;
        private string pseudo1;
        private string IPGame;
        public int nbJoueurs;
        private int nbChoix = 0; //on augmente le chiffre a chaque fois qu'un joueur valide un avatar pour aller sur la prochaine page quand nbChoix == nbJoueurs
        //private string[] tabAvatar = {"Yuno","Rem","2B","Nezuko","ZeroTwo"};
        private List<string> listAvatar = new List<string>();
        private int tmpAvatar = 0;
        private int tmpAvatarMax = 4;

        public Joueur joueurLocal = new Joueur();
        public List<Joueur> listJoueurs = new List<Joueur>();



        public ChoixPerso(CreationLobby Crea)
        {
            InitializeComponent();
            this._Crea = Crea;
            this.IPGame = this._Crea.GetIP();
            this.IPServer.Text = this.IPGame;
            this.nbJoueurs = PhaseLobby.DemandeNbJoueur();
            this.tServer = this._Crea.tServer;
            this.pseudo1 = this._Crea.GetPseudo();
            this.joueurLocal.Nom = pseudo1;

            switch (nbJoueurs)
            {
                case 2:
                    this.Pseudo2.Visible = true;
                    break;

                case 3:
                    this.Pseudo2.Visible = true; 
                    this.Pseudo3.Visible = true;
                    break;

                case 4:
                    this.Pseudo2.Visible = true;
                    this.Pseudo3.Visible = true;
                    this.Pseudo4.Visible = true;
                    break;

            }
            this.listAvatar.Add("Yuno");
            this.listAvatar.Add("Rem");
            this.listAvatar.Add("2B");
            this.listAvatar.Add("Nezuko");
            this.listAvatar.Add("ZeroTwo");
            this.pictBoxAvatar.Image = QOT_UserInterface.Properties.Resources.Yuno;
        }


        public ChoixPerso(string ip, string nom)
        {
            InitializeComponent();

            this.IPGame = ip;
            this.IPServer.Text = this.IPGame;
            this.nbJoueurs = PhaseLobby.DemandeNbJoueur();
            this.joueurLocal.Nom = nom;

            switch (nbJoueurs)
            {
                case 2:
                    this.Pseudo2.Visible = true;
                    break;

                case 3:
                    this.Pseudo2.Visible = true;
                    this.Pseudo3.Visible = true;
                    break;

                case 4:
                    this.Pseudo2.Visible = true;
                    this.Pseudo3.Visible = true;
                    this.Pseudo4.Visible = true;
                    break;

            }
            this.listAvatar.Add("Yuno");
            this.listAvatar.Add("Rem");
            this.listAvatar.Add("2B");
            this.listAvatar.Add("Nezuko");
            this.listAvatar.Add("ZeroTwo");
            this.pictBoxAvatar.Image = QOT_UserInterface.Properties.Resources.Yuno;

        }



        private void ChangeLabelInThread(Label lbl, string text)
        {
            MethodInvoker invoker = delegate
            {
                lbl.Text = text;
                lbl.Visible = true;
            };
            this.Invoke(invoker);
        }

        private void SetFormInvisibleInThread(Form f)
        {
            MethodInvoker invoker = delegate
            {
                f.Visible = false;
            };
            this.Invoke(invoker);
        }

        private void ThreadAjoutNomAvatar()
        {
            Dictionary<string, string> listNoms = new Dictionary<string, string>();
       
            while (listNoms.Count != nbJoueurs || listNoms.Values.ToList().Contains("Choix en cours..."))
            {
                listNoms = PhaseLobby.ReceptionNomsAvatars();
                if (listNoms.Count == 1)
                {
                    ChangeLabelInThread(Pseudo1, listNoms.Keys.ToList()[0]);
                    ChangeLabelInThread(Avatar1, listNoms.Values.ToList()[0]);
                }
                if (listNoms.Count == 2)
                {
                    ChangeLabelInThread(Pseudo1, listNoms.Keys.ToList()[0]);
                    ChangeLabelInThread(Avatar1, listNoms.Values.ToList()[0]);

                    ChangeLabelInThread(Pseudo2, listNoms.Keys.ToList()[1]);
                    ChangeLabelInThread(Avatar2, listNoms.Values.ToList()[1]);
                }
                if (listNoms.Count == 3)
                {
                    ChangeLabelInThread(Pseudo1, listNoms.Keys.ToList()[0]);
                    ChangeLabelInThread(Avatar1, listNoms.Values.ToList()[0]);

                    ChangeLabelInThread(Pseudo2, listNoms.Keys.ToList()[1]);
                    ChangeLabelInThread(Avatar2, listNoms.Values.ToList()[1]);

                    ChangeLabelInThread(Pseudo3, listNoms.Keys.ToList()[2]);
                    ChangeLabelInThread(Avatar3, listNoms.Values.ToList()[2]);
                }
                if (listNoms.Count == 4)
                {
                    ChangeLabelInThread(Pseudo1, listNoms.Keys.ToList()[0]);
                    ChangeLabelInThread(Avatar1, listNoms.Values.ToList()[0]);

                    ChangeLabelInThread(Pseudo2, listNoms.Keys.ToList()[1]);
                    ChangeLabelInThread(Avatar2, listNoms.Values.ToList()[1]);

                    ChangeLabelInThread(Pseudo3, listNoms.Keys.ToList()[2]);
                    ChangeLabelInThread(Avatar3, listNoms.Values.ToList()[2]);

                    ChangeLabelInThread(Pseudo4, listNoms.Keys.ToList()[3]);
                    ChangeLabelInThread(Avatar4, listNoms.Values.ToList()[3]);
                }
            }
            RemplissageListeJoueurs(listNoms);

        }

        private void RemplissageListeJoueurs(Dictionary<string, string> listNoms)
        {
            foreach (var pair in listNoms)
            {
                Joueur newJ = new Joueur(pair.Key);
                newJ.NomAvatar = pair.Value;
                listJoueurs.Add(newJ);
            }
        }


        public int GetNbJoueurs()
        {
            return this.nbJoueurs;
        }


        private void ChoixPerso_Load(object sender, EventArgs e)
        {
            Application.ApplicationExit += new EventHandler(Service_Infrastructure.ClearConnexion);
            tAjoutNomAv = Task.Factory.StartNew(() => ThreadAjoutNomAvatar());
        }

        private void label87_Click(object sender, EventArgs e)
        {

        }

        private void RejoindreLobby_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (this.ActiveControl.Text != "Retour")
            {
                Application.Exit();
            }
        }

        private async void Valider_Click(object sender, EventArgs e)
        {
            string nomAvatar = listAvatar[tmpAvatar];
            if (!PhaseLobby.AvatarChoisi(nomAvatar))
            {
                MessageBox.Show("Avatar déjà choisi par un autre joueur.");
            }
            else
            {
                this.joueurLocal.NomAvatar = nomAvatar;
                Valider.Enabled = false;

                await tAjoutNomAv;

                joueurLocal.Nom = PhaseLobby.MonNom();

                PhaseLobby.FinPhase();
                DebutPartie debPartie = new DebutPartie(this);
                this.Visible = false;
                debPartie.ShowDialog();
            }
        }

        private void pictBoxAvatar_Click(object sender, EventArgs e)
        {

        }

        private void pictureBox2_Click(object sender, EventArgs e)
        {
            if(tmpAvatar == tmpAvatarMax)
            {
                tmpAvatar = 0;
            }
            else
            {
                tmpAvatar++;
            }

            ChangerAvatar(tmpAvatar);
                

        }
        
        private void pictureBox1_Click(object sender, EventArgs e)
        {
            if (tmpAvatar == 0)
            {
                tmpAvatar = tmpAvatarMax;
            }
            else
            {
                tmpAvatar--;
            }

            ChangerAvatar(tmpAvatar);
        }

        private void ChangerAvatar(int tmp)
        {
            switch (listAvatar[tmp])
            {
                case "Yuno":
                    this.pictBoxAvatar.Image = QOT_UserInterface.Properties.Resources.Yuno;
                    break;
                case "Rem":
                    this.pictBoxAvatar.Image = QOT_UserInterface.Properties.Resources.Rem;
                    break;
                case "2B":
                    this.pictBoxAvatar.Image = QOT_UserInterface.Properties.Resources._2B;
                    break;
                case "Nezuko":
                    this.pictBoxAvatar.Image = QOT_UserInterface.Properties.Resources.Nezuko;
                    break;
                case "ZeroTwo":
                    this.pictBoxAvatar.Image = QOT_UserInterface.Properties.Resources.ZeroTwo;
                    break;

            }
        }

        
    }
}

﻿namespace QOT_UserInterface
{
    partial class CreationLobby
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblNbJoueurs = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label87 = new System.Windows.Forms.Label();
            this.lblMonIP = new System.Windows.Forms.Label();
            this.btnCreerLobby = new System.Windows.Forms.Button();
            this.Retour = new System.Windows.Forms.Button();
            this.numUpDownNbJoueurs = new System.Windows.Forms.NumericUpDown();
            this.lblPseudo = new System.Windows.Forms.Label();
            this.pictBLogo = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.numUpDownNbJoueurs)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictBLogo)).BeginInit();
            this.SuspendLayout();
            // 
            // lblNbJoueurs
            // 
            this.lblNbJoueurs.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.lblNbJoueurs.AutoSize = true;
            this.lblNbJoueurs.Location = new System.Drawing.Point(301, 338);
            this.lblNbJoueurs.Name = "lblNbJoueurs";
            this.lblNbJoueurs.Size = new System.Drawing.Size(105, 13);
            this.lblNbJoueurs.TabIndex = 3;
            this.lblNbJoueurs.Text = "Nombre de joueurs : ";
            // 
            // label1
            // 
            this.label1.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(464, 338);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(74, 13);
            this.label1.TabIndex = 4;
            this.label1.Text = "(2 à 4 joueurs)";
            // 
            // label87
            // 
            this.label87.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.label87.AutoSize = true;
            this.label87.Location = new System.Drawing.Point(308, 377);
            this.label87.Name = "label87";
            this.label87.Size = new System.Drawing.Size(90, 13);
            this.label87.TabIndex = 6;
            this.label87.Text = "Mon adresse IP : ";
            // 
            // lblMonIP
            // 
            this.lblMonIP.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.lblMonIP.AutoSize = true;
            this.lblMonIP.Location = new System.Drawing.Point(409, 377);
            this.lblMonIP.Name = "lblMonIP";
            this.lblMonIP.Size = new System.Drawing.Size(35, 13);
            this.lblMonIP.TabIndex = 7;
            this.lblMonIP.Text = "label2";
            this.lblMonIP.Click += new System.EventHandler(this.lblMonIP_Click);
            // 
            // btnCreerLobby
            // 
            this.btnCreerLobby.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.btnCreerLobby.Location = new System.Drawing.Point(383, 430);
            this.btnCreerLobby.Name = "btnCreerLobby";
            this.btnCreerLobby.Size = new System.Drawing.Size(218, 53);
            this.btnCreerLobby.TabIndex = 8;
            this.btnCreerLobby.Text = "Créer le lobby";
            this.btnCreerLobby.UseVisualStyleBackColor = true;
            this.btnCreerLobby.Click += new System.EventHandler(this.btnCreerLobby_Click);
            // 
            // Retour
            // 
            this.Retour.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.Retour.Location = new System.Drawing.Point(848, 576);
            this.Retour.Name = "Retour";
            this.Retour.Size = new System.Drawing.Size(122, 34);
            this.Retour.TabIndex = 9;
            this.Retour.Text = "Retour";
            this.Retour.UseVisualStyleBackColor = true;
            this.Retour.Click += new System.EventHandler(this.Retour_Click);
            // 
            // numUpDownNbJoueurs
            // 
            this.numUpDownNbJoueurs.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.numUpDownNbJoueurs.Location = new System.Drawing.Point(414, 336);
            this.numUpDownNbJoueurs.Maximum = new decimal(new int[] {
            4,
            0,
            0,
            0});
            this.numUpDownNbJoueurs.Minimum = new decimal(new int[] {
            2,
            0,
            0,
            0});
            this.numUpDownNbJoueurs.Name = "numUpDownNbJoueurs";
            this.numUpDownNbJoueurs.Size = new System.Drawing.Size(30, 20);
            this.numUpDownNbJoueurs.TabIndex = 10;
            this.numUpDownNbJoueurs.ThousandsSeparator = true;
            this.numUpDownNbJoueurs.Value = new decimal(new int[] {
            2,
            0,
            0,
            0});
            this.numUpDownNbJoueurs.ValueChanged += new System.EventHandler(this.numUpDownNbJoueurs_ValueChanged);
            // 
            // lblPseudo
            // 
            this.lblPseudo.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.lblPseudo.AutoSize = true;
            this.lblPseudo.Location = new System.Drawing.Point(486, 277);
            this.lblPseudo.Name = "lblPseudo";
            this.lblPseudo.Size = new System.Drawing.Size(35, 13);
            this.lblPseudo.TabIndex = 11;
            this.lblPseudo.Text = "label2";
            // 
            // pictBLogo
            // 
            this.pictBLogo.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.pictBLogo.Image = global::QOT_UserInterface.Properties.Resources.logo;
            this.pictBLogo.Location = new System.Drawing.Point(208, 42);
            this.pictBLogo.Name = "pictBLogo";
            this.pictBLogo.Size = new System.Drawing.Size(656, 182);
            this.pictBLogo.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.pictBLogo.TabIndex = 1;
            this.pictBLogo.TabStop = false;
            // 
            // CreationLobby
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(1064, 681);
            this.Controls.Add(this.lblPseudo);
            this.Controls.Add(this.numUpDownNbJoueurs);
            this.Controls.Add(this.Retour);
            this.Controls.Add(this.btnCreerLobby);
            this.Controls.Add(this.lblMonIP);
            this.Controls.Add(this.label87);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.lblNbJoueurs);
            this.Controls.Add(this.pictBLogo);
            this.Name = "CreationLobby";
            this.Text = "CreationLobby";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.CreationLobby_FormClosing);
            this.Load += new System.EventHandler(this.CreationLobby_Load);
            ((System.ComponentModel.ISupportInitialize)(this.numUpDownNbJoueurs)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictBLogo)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox pictBLogo;
        private System.Windows.Forms.Label lblNbJoueurs;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label87;
        private System.Windows.Forms.Label lblMonIP;
        private System.Windows.Forms.Button btnCreerLobby;
        private System.Windows.Forms.Button Retour;
        private System.Windows.Forms.NumericUpDown numUpDownNbJoueurs;
        private System.Windows.Forms.Label lblPseudo;
    }
}
﻿using QOT_Application;
using QueenOfTokyo_Infrastructure;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace QOT_UserInterface
{
    public partial class CreationLobby : Form
    {
        private Accueil _Acc;
        private string pseudo1;
        public Thread tServer;

        public CreationLobby(Accueil Acc)
        {
            InitializeComponent();
            this._Acc = Acc;
            this.pseudo1 = this._Acc.txtPseudo.Text;
            lblMonIP.Text = Service_Infrastructure.GetLocalIPAddress();
            this.lblPseudo.Text = this.pseudo1;
            
        }

        public string GetPseudo()
        {
            return this.pseudo1;
        }

        public string GetIP()
        {
            return this.lblMonIP.Text;
        }

        public int GetnumJoueurs()
        {
            return Convert.ToInt32(this.numUpDownNbJoueurs.Value);
        }

        private void Retour_Click(object sender, EventArgs e)
        {
            this._Acc.Visible = true;
            this.Close();
        }

        private void CreationLobby_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (this.ActiveControl.Text != "Retour")
            {
                Application.Exit();
            }
        }

        private void btnCreerLobby_Click(object sender, EventArgs e)
        {
            int nbJoueurs = (int)numUpDownNbJoueurs.Value;
            tServer = new Thread(new ParameterizedThreadStart(HostServer.ServerInit));
            tServer.Start(nbJoueurs);

            lblPseudo.Text = PhaseLobby.Connexion(lblMonIP.Text, lblPseudo.Text);

            ChoixPerso choixPerso = new ChoixPerso(this);
            this.Visible = false;

            choixPerso.ShowDialog();
        }

        private void txtNbJoueurs_TextChanged(object sender, EventArgs e)
        {

        }

        private void lblMonIP_Click(object sender, EventArgs e)
        {

        }

        private void CreationLobby_Load(object sender, EventArgs e)
        {

        }

        private void numUpDownNbJoueurs_ValueChanged(object sender, EventArgs e)
        {

        }
    }
}

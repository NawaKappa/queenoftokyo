﻿namespace QOT_UserInterface
{
    partial class DebutPartie
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnCommencer = new System.Windows.Forms.Button();
            this.lblJoueur1 = new System.Windows.Forms.Label();
            this.lblJoueur2 = new System.Windows.Forms.Label();
            this.lblJoueur3 = new System.Windows.Forms.Label();
            this.lblJoueur4 = new System.Windows.Forms.Label();
            this.pictJoueur4 = new System.Windows.Forms.PictureBox();
            this.pictDeJoueur4 = new System.Windows.Forms.PictureBox();
            this.pictJoueur3 = new System.Windows.Forms.PictureBox();
            this.pictDeJoueur3 = new System.Windows.Forms.PictureBox();
            this.pictJoueur2 = new System.Windows.Forms.PictureBox();
            this.pictDeJoueur2 = new System.Windows.Forms.PictureBox();
            this.pictJoueur1 = new System.Windows.Forms.PictureBox();
            this.pictDeJoueur1 = new System.Windows.Forms.PictureBox();
            this.pictBLogo = new System.Windows.Forms.PictureBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.gbJ2 = new System.Windows.Forms.GroupBox();
            this.gbJ3 = new System.Windows.Forms.GroupBox();
            this.gbJ4 = new System.Windows.Forms.GroupBox();
            ((System.ComponentModel.ISupportInitialize)(this.pictJoueur4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictDeJoueur4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictJoueur3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictDeJoueur3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictJoueur2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictDeJoueur2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictJoueur1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictDeJoueur1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictBLogo)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.gbJ2.SuspendLayout();
            this.gbJ3.SuspendLayout();
            this.gbJ4.SuspendLayout();
            this.SuspendLayout();
            // 
            // btnCommencer
            // 
            this.btnCommencer.Location = new System.Drawing.Point(394, 624);
            this.btnCommencer.Name = "btnCommencer";
            this.btnCommencer.Size = new System.Drawing.Size(292, 45);
            this.btnCommencer.TabIndex = 14;
            this.btnCommencer.Text = "Commencer";
            this.btnCommencer.UseVisualStyleBackColor = true;
            this.btnCommencer.Click += new System.EventHandler(this.btnLancerJoueur4_Click);
            // 
            // lblJoueur1
            // 
            this.lblJoueur1.AutoSize = true;
            this.lblJoueur1.Location = new System.Drawing.Point(88, 84);
            this.lblJoueur1.Name = "lblJoueur1";
            this.lblJoueur1.Size = new System.Drawing.Size(55, 13);
            this.lblJoueur1.TabIndex = 15;
            this.lblJoueur1.Text = "lblJoueur1";
            // 
            // lblJoueur2
            // 
            this.lblJoueur2.AutoSize = true;
            this.lblJoueur2.Location = new System.Drawing.Point(91, 84);
            this.lblJoueur2.Name = "lblJoueur2";
            this.lblJoueur2.Size = new System.Drawing.Size(55, 13);
            this.lblJoueur2.TabIndex = 16;
            this.lblJoueur2.Text = "lblJoueur2";
            // 
            // lblJoueur3
            // 
            this.lblJoueur3.AutoSize = true;
            this.lblJoueur3.Location = new System.Drawing.Point(90, 84);
            this.lblJoueur3.Name = "lblJoueur3";
            this.lblJoueur3.Size = new System.Drawing.Size(55, 13);
            this.lblJoueur3.TabIndex = 17;
            this.lblJoueur3.Text = "lblJoueur3";
            // 
            // lblJoueur4
            // 
            this.lblJoueur4.AutoSize = true;
            this.lblJoueur4.Location = new System.Drawing.Point(90, 84);
            this.lblJoueur4.Name = "lblJoueur4";
            this.lblJoueur4.Size = new System.Drawing.Size(55, 13);
            this.lblJoueur4.TabIndex = 18;
            this.lblJoueur4.Text = "lblJoueur4";
            this.lblJoueur4.Click += new System.EventHandler(this.lblJoueur4_Click);
            // 
            // pictJoueur4
            // 
            this.pictJoueur4.Location = new System.Drawing.Point(15, 100);
            this.pictJoueur4.Name = "pictJoueur4";
            this.pictJoueur4.Size = new System.Drawing.Size(208, 250);
            this.pictJoueur4.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictJoueur4.TabIndex = 13;
            this.pictJoueur4.TabStop = false;
            // 
            // pictDeJoueur4
            // 
            this.pictDeJoueur4.Location = new System.Drawing.Point(93, 17);
            this.pictDeJoueur4.Name = "pictDeJoueur4";
            this.pictDeJoueur4.Size = new System.Drawing.Size(51, 50);
            this.pictDeJoueur4.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictDeJoueur4.TabIndex = 12;
            this.pictDeJoueur4.TabStop = false;
            // 
            // pictJoueur3
            // 
            this.pictJoueur3.Location = new System.Drawing.Point(15, 100);
            this.pictJoueur3.Name = "pictJoueur3";
            this.pictJoueur3.Size = new System.Drawing.Size(208, 250);
            this.pictJoueur3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictJoueur3.TabIndex = 10;
            this.pictJoueur3.TabStop = false;
            // 
            // pictDeJoueur3
            // 
            this.pictDeJoueur3.Location = new System.Drawing.Point(94, 17);
            this.pictDeJoueur3.Name = "pictDeJoueur3";
            this.pictDeJoueur3.Size = new System.Drawing.Size(51, 50);
            this.pictDeJoueur3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictDeJoueur3.TabIndex = 9;
            this.pictDeJoueur3.TabStop = false;
            // 
            // pictJoueur2
            // 
            this.pictJoueur2.Location = new System.Drawing.Point(15, 100);
            this.pictJoueur2.Name = "pictJoueur2";
            this.pictJoueur2.Size = new System.Drawing.Size(208, 250);
            this.pictJoueur2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictJoueur2.TabIndex = 7;
            this.pictJoueur2.TabStop = false;
            // 
            // pictDeJoueur2
            // 
            this.pictDeJoueur2.Location = new System.Drawing.Point(94, 17);
            this.pictDeJoueur2.Name = "pictDeJoueur2";
            this.pictDeJoueur2.Size = new System.Drawing.Size(52, 50);
            this.pictDeJoueur2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictDeJoueur2.TabIndex = 6;
            this.pictDeJoueur2.TabStop = false;
            // 
            // pictJoueur1
            // 
            this.pictJoueur1.Location = new System.Drawing.Point(15, 100);
            this.pictJoueur1.Name = "pictJoueur1";
            this.pictJoueur1.Size = new System.Drawing.Size(208, 250);
            this.pictJoueur1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictJoueur1.TabIndex = 4;
            this.pictJoueur1.TabStop = false;
            // 
            // pictDeJoueur1
            // 
            this.pictDeJoueur1.Location = new System.Drawing.Point(91, 17);
            this.pictDeJoueur1.Name = "pictDeJoueur1";
            this.pictDeJoueur1.Size = new System.Drawing.Size(52, 50);
            this.pictDeJoueur1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictDeJoueur1.TabIndex = 3;
            this.pictDeJoueur1.TabStop = false;
            // 
            // pictBLogo
            // 
            this.pictBLogo.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.pictBLogo.Image = global::QOT_UserInterface.Properties.Resources.logo;
            this.pictBLogo.Location = new System.Drawing.Point(208, 29);
            this.pictBLogo.Name = "pictBLogo";
            this.pictBLogo.Size = new System.Drawing.Size(656, 182);
            this.pictBLogo.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.pictBLogo.TabIndex = 2;
            this.pictBLogo.TabStop = false;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.pictDeJoueur1);
            this.groupBox1.Controls.Add(this.pictJoueur1);
            this.groupBox1.Controls.Add(this.lblJoueur1);
            this.groupBox1.Location = new System.Drawing.Point(12, 229);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(238, 371);
            this.groupBox1.TabIndex = 19;
            this.groupBox1.TabStop = false;
            // 
            // gbJ2
            // 
            this.gbJ2.Controls.Add(this.pictJoueur2);
            this.gbJ2.Controls.Add(this.pictDeJoueur2);
            this.gbJ2.Controls.Add(this.lblJoueur2);
            this.gbJ2.Location = new System.Drawing.Point(279, 229);
            this.gbJ2.Name = "gbJ2";
            this.gbJ2.Size = new System.Drawing.Size(241, 371);
            this.gbJ2.TabIndex = 20;
            this.gbJ2.TabStop = false;
            // 
            // gbJ3
            // 
            this.gbJ3.Controls.Add(this.pictJoueur3);
            this.gbJ3.Controls.Add(this.pictDeJoueur3);
            this.gbJ3.Controls.Add(this.lblJoueur3);
            this.gbJ3.Location = new System.Drawing.Point(541, 229);
            this.gbJ3.Name = "gbJ3";
            this.gbJ3.Size = new System.Drawing.Size(238, 371);
            this.gbJ3.TabIndex = 21;
            this.gbJ3.TabStop = false;
            this.gbJ3.Visible = false;
            // 
            // gbJ4
            // 
            this.gbJ4.Controls.Add(this.pictJoueur4);
            this.gbJ4.Controls.Add(this.pictDeJoueur4);
            this.gbJ4.Controls.Add(this.lblJoueur4);
            this.gbJ4.Location = new System.Drawing.Point(800, 229);
            this.gbJ4.Name = "gbJ4";
            this.gbJ4.Size = new System.Drawing.Size(238, 371);
            this.gbJ4.TabIndex = 22;
            this.gbJ4.TabStop = false;
            this.gbJ4.Visible = false;
            // 
            // DebutPartie
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(1064, 681);
            this.Controls.Add(this.gbJ4);
            this.Controls.Add(this.gbJ3);
            this.Controls.Add(this.btnCommencer);
            this.Controls.Add(this.gbJ2);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.pictBLogo);
            this.Name = "DebutPartie";
            this.Text = "DebutPartie";
            this.Load += new System.EventHandler(this.DebutPartie_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pictJoueur4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictDeJoueur4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictJoueur3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictDeJoueur3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictJoueur2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictDeJoueur2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictJoueur1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictDeJoueur1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictBLogo)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.gbJ2.ResumeLayout(false);
            this.gbJ2.PerformLayout();
            this.gbJ3.ResumeLayout(false);
            this.gbJ3.PerformLayout();
            this.gbJ4.ResumeLayout(false);
            this.gbJ4.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox pictBLogo;
        private System.Windows.Forms.PictureBox pictDeJoueur1;
        private System.Windows.Forms.PictureBox pictJoueur1;
        private System.Windows.Forms.PictureBox pictJoueur2;
        private System.Windows.Forms.PictureBox pictDeJoueur2;
        private System.Windows.Forms.PictureBox pictJoueur3;
        private System.Windows.Forms.PictureBox pictDeJoueur3;
        private System.Windows.Forms.Button btnCommencer;
        private System.Windows.Forms.PictureBox pictJoueur4;
        private System.Windows.Forms.PictureBox pictDeJoueur4;
        private System.Windows.Forms.Label lblJoueur1;
        private System.Windows.Forms.Label lblJoueur2;
        private System.Windows.Forms.Label lblJoueur3;
        private System.Windows.Forms.Label lblJoueur4;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox gbJ2;
        private System.Windows.Forms.GroupBox gbJ3;
        private System.Windows.Forms.GroupBox gbJ4;
    }
}
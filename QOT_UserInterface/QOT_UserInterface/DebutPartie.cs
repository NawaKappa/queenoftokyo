﻿using QOT_Application;
using QOT_Domain_Joueur;
using QueenOfTokyo_Infrastructure;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace QOT_UserInterface
{
    public partial class DebutPartie : Form
    {
        private ChoixPerso _ChPerso;

        public Joueur joueurLocal = new Joueur();
        public List<Joueur> listJoueur = new List<Joueur>();

        private int nbJoueurs;
        private int tmpNbJoueurs = 0; //+1 quand un joueur lance le dé. Quand tmpNbJoueur == nbJoueurs, on lance le Plateau

        public DebutPartie(ChoixPerso ChPerso)
        {
            InitializeComponent();
            this._ChPerso = ChPerso;
            this.joueurLocal = ChPerso.joueurLocal;
            this.listJoueur = ChPerso.listJoueurs;
            this.nbJoueurs = this._ChPerso.GetNbJoueurs();
            this.btnCommencer.Enabled = false;
            this.lblJoueur1.Text = listJoueur[0].Nom;
            this.lblJoueur2.Text = listJoueur[1].Nom;
            if (nbJoueurs >= 3)
                this.lblJoueur3.Text = listJoueur[2].Nom;
            if (nbJoueurs >= 4)
                this.lblJoueur4.Text = listJoueur[3].Nom;

            switch (nbJoueurs)
            {
                case 2:
                    this.gbJ2.Visible = true;
                    break;

                case 3:
                    this.gbJ2.Visible = true;
                    this.gbJ3.Visible = true;
                    break;

                case 4:
                    this.gbJ2.Visible = true;
                    this.gbJ3.Visible = true;
                    this.gbJ4.Visible = true;
                    break;

            }
        }

        public int GetNbJoueurs()
        {
            return this.nbJoueurs;
        }

        private void RandomDes(PictureBox pctB)
        {
            Random rdn = new Random();
            int resultat = rdn.Next(0, 6);
            SetDesIHM(pctB, resultat);
        }


        private void ChangeButtonInThread(Button btn)
        {
            MethodInvoker invoker = delegate
            {
                btn.Enabled = true;
            };
            this.Invoke(invoker);
        }


        private void EffetRandomDes()
        {
            for(int i = 0; i<100; i++)
            {
                RandomDes(pictDeJoueur1);
                RandomDes(pictDeJoueur2);
                RandomDes(pictDeJoueur3);
                RandomDes(pictDeJoueur4);
                System.Threading.Thread.Sleep(20);
            }
            SetAllDesIHM();
            ChangeButtonInThread(btnCommencer);
            Client.SendToServer("END");

        }



        private void SetAvatarIHM(PictureBox pctB, Joueur j)
        {
            if (j.NomAvatar == "Yuno")
                pctB.Image = Properties.Resources.Yuno;
            if (j.NomAvatar == "ZeroTwo")
                pctB.Image = Properties.Resources.ZeroTwo;
            if (j.NomAvatar == "2B")
                pctB.Image = Properties.Resources._2B;
            if (j.NomAvatar == "Rem")
                pctB.Image = Properties.Resources.Rem;
            if (j.NomAvatar == "Nezuko")
                pctB.Image = Properties.Resources.Nezuko;
        }

        private void SetAllAvatarsIHM()
        {
            SetAvatarIHM(pictJoueur1, listJoueur[0]);
            SetAvatarIHM(pictJoueur2, listJoueur[1]);
            if (nbJoueurs >= 3)
                SetAvatarIHM(pictJoueur3, listJoueur[2]);
            if (nbJoueurs >= 4)
                SetAvatarIHM(pictJoueur4, listJoueur[3]);
        }


        private void lblJoueur4_Click(object sender, EventArgs e)
        {

        }

        private void DebutPartie_Load(object sender, EventArgs e)
        {
            //Task.Factory.StartNew(() =>PhaseDebutPartie.ServerDebutPartie());
            Application.ApplicationExit += new EventHandler(Service_Infrastructure.ClearConnexion);

            Thread tDebPar = new Thread(new ThreadStart(PhaseDebutPartie.ServerDebutPartie));
            tDebPar.Start();

            listJoueur = PhaseDebutPartie.ResultatsLanceDes();

            Thread tEffetDes = new Thread(new ThreadStart(EffetRandomDes));
            tEffetDes.Start();

            SetAllAvatarsIHM();

        }


        private void SetAllDesIHM()
        {
            SetDesIHM(pictDeJoueur1, listJoueur[0].DesDebut);
            SetDesIHM(pictDeJoueur2, listJoueur[1].DesDebut);
            if(nbJoueurs >= 3)
                SetDesIHM(pictDeJoueur3, listJoueur[2].DesDebut);
            if(nbJoueurs >= 4)
                SetDesIHM(pictDeJoueur4, listJoueur[3].DesDebut);
        }

        private void SetDesIHM(PictureBox pctb, int resultat)
        {
            if (resultat == 1)
                pctb.Image = Properties.Resources.debDe1;
            if (resultat == 2)
                pctb.Image = Properties.Resources.debDe2;
            if (resultat == 3)
                pctb.Image = Properties.Resources.debDe3;
            if (resultat == 4)
                pctb.Image = Properties.Resources.debDe4;
            if (resultat == 5)
                pctb.Image = Properties.Resources.debDe5;
            if (resultat == 6)
                pctb.Image = Properties.Resources.debDe6;
        }


        private void btnLancerJoueur4_Click(object sender, EventArgs e)
        {
            this.btnCommencer.Enabled = false;

            joueurLocal = listJoueur.Find(j => j.Nom == joueurLocal.Nom);
            listJoueur = listJoueur.OrderBy(j => j.DesDebut).Reverse().ToList();

            Plateau plateau = new Plateau(this);
            this.Visible = false;
            plateau.ShowDialog();
        }
    }
}

﻿namespace QOT_UserInterface
{
    partial class InfosCartes
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Carte3 = new System.Windows.Forms.PictureBox();
            this.Carte2 = new System.Windows.Forms.PictureBox();
            this.Carte1 = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.Carte3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Carte2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Carte1)).BeginInit();
            this.SuspendLayout();
            // 
            // Carte3
            // 
            this.Carte3.Image = global::QOT_UserInterface.Properties.Resources.backCard;
            this.Carte3.Location = new System.Drawing.Point(537, 34);
            this.Carte3.Name = "Carte3";
            this.Carte3.Size = new System.Drawing.Size(215, 325);
            this.Carte3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.Carte3.TabIndex = 2;
            this.Carte3.TabStop = false;
            // 
            // Carte2
            // 
            this.Carte2.Image = global::QOT_UserInterface.Properties.Resources.backCard;
            this.Carte2.Location = new System.Drawing.Point(286, 34);
            this.Carte2.Name = "Carte2";
            this.Carte2.Size = new System.Drawing.Size(218, 325);
            this.Carte2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.Carte2.TabIndex = 1;
            this.Carte2.TabStop = false;
            // 
            // Carte1
            // 
            this.Carte1.Image = global::QOT_UserInterface.Properties.Resources.backCard;
            this.Carte1.Location = new System.Drawing.Point(30, 34);
            this.Carte1.Name = "Carte1";
            this.Carte1.Size = new System.Drawing.Size(220, 325);
            this.Carte1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.Carte1.TabIndex = 0;
            this.Carte1.TabStop = false;
            // 
            // InfosCartes
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.ClientSize = new System.Drawing.Size(772, 402);
            this.Controls.Add(this.Carte3);
            this.Controls.Add(this.Carte2);
            this.Controls.Add(this.Carte1);
            this.Name = "InfosCartes";
            this.Text = "InfosCartes";
            this.Load += new System.EventHandler(this.InfosCartes_Load);
            ((System.ComponentModel.ISupportInitialize)(this.Carte3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Carte2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Carte1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.PictureBox Carte1;
        private System.Windows.Forms.PictureBox Carte2;
        private System.Windows.Forms.PictureBox Carte3;
    }
}
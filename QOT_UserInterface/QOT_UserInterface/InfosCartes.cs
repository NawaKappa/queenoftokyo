﻿using QOT_Domain_Carte;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace QOT_UserInterface
{
    public partial class InfosCartes : Form
    {
        public List<Carte> _listCartes;

        public InfosCartes(List<Carte> listCartes)
        {
            InitializeComponent();
            this._listCartes = listCartes;
        }

        private void InfosCartes_Load(object sender, EventArgs e)
        {
            UpdateIHMCartes();
        }



        private void ChangePictureBoxCarteInThread(PictureBox pctB, Carte c)
        {
            MethodInvoker invoker = delegate
            {
                if (c == null)
                {
                    pctB.Image = Properties.Resources.backCard;
                }
                else
                {
                    pctB.Image = WhichCarteIHM(c);
                }
            };
            this.Invoke(invoker);
        }


        private Bitmap WhichCarteIHM(Carte c)
        {
            if (c.Nom == "Soin")
                return Properties.Resources.Soin;
            if (c.Nom == "Tramway")
                return Properties.Resources.Tramway;
            if (c.Nom == "Recharge")
                return Properties.Resources.Recharge;
            if (c.Nom == "Restauration")
                return Properties.Resources.Restauration;
            if (c.Nom == "Evacuation")
                return Properties.Resources.Evacuation;
            if (c.Nom == "Gaz")
                return Properties.Resources.Gaz;
            if (c.Nom == "Lanceflamme")
                return Properties.Resources.Lance_Flamme;
            if (c.Nom == "AmiEnfants")
                return Properties.Resources.Ami_des_Enfants;
            if (c.Nom == "Acide")
                return Properties.Resources.Attaque_acide;
            if (c.Nom == "Regeneration")
                return Properties.Resources.Regeneration;
            else
                throw new Exception();
        }

        private void UpdateIHMCartes()
        {
            if (_listCartes.Count > 0)
            {
                ChangePictureBoxCarteInThread(Carte1, _listCartes[0]);
                ChangePictureBoxCarteInThread(Carte2, null);
                ChangePictureBoxCarteInThread(Carte3, null);

                if (_listCartes.Count > 1)
                {
                    ChangePictureBoxCarteInThread(Carte2, _listCartes[1]);
                    ChangePictureBoxCarteInThread(Carte3, null);
                }

                if (_listCartes.Count > 2)
                {
                    ChangePictureBoxCarteInThread(Carte3, _listCartes[2]);
                }
            }
        }

    }
}

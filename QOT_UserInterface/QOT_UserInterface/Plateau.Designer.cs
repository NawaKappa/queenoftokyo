﻿namespace QOT_UserInterface
{
    partial class Plateau
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.checkBox9 = new System.Windows.Forms.CheckBox();
            this.checkBox8 = new System.Windows.Forms.CheckBox();
            this.checkBox7 = new System.Windows.Forms.CheckBox();
            this.checkBox6 = new System.Windows.Forms.CheckBox();
            this.checkBox5 = new System.Windows.Forms.CheckBox();
            this.checkBox4 = new System.Windows.Forms.CheckBox();
            this.checkBox3 = new System.Windows.Forms.CheckBox();
            this.checkBox2 = new System.Windows.Forms.CheckBox();
            this.checkBox1 = new System.Windows.Forms.CheckBox();
            this.label22 = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.pictRefillShop = new System.Windows.Forms.PictureBox();
            this.pictShop3 = new System.Windows.Forms.PictureBox();
            this.pictShop1 = new System.Windows.Forms.PictureBox();
            this.pictShop2 = new System.Windows.Forms.PictureBox();
            this.pictDe6 = new System.Windows.Forms.PictureBox();
            this.pictDe5 = new System.Windows.Forms.PictureBox();
            this.pictDe4 = new System.Windows.Forms.PictureBox();
            this.pictDe3 = new System.Windows.Forms.PictureBox();
            this.pictDe2 = new System.Windows.Forms.PictureBox();
            this.pictDe1 = new System.Windows.Forms.PictureBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.checkBox12 = new System.Windows.Forms.CheckBox();
            this.checkBox11 = new System.Windows.Forms.CheckBox();
            this.checkBox10 = new System.Windows.Forms.CheckBox();
            this.btnFinTour = new System.Windows.Forms.Button();
            this.btnLancer = new System.Windows.Forms.Button();
            this.BtnAcheter = new System.Windows.Forms.Button();
            this.btnVendre = new System.Windows.Forms.Button();
            this.CarteJoueurActuel3 = new System.Windows.Forms.PictureBox();
            this.lblNbPVJActuel = new System.Windows.Forms.Label();
            this.lblNbEneJActuel = new System.Windows.Forms.Label();
            this.lblNbVicJActuel = new System.Windows.Forms.Label();
            this.label24 = new System.Windows.Forms.Label();
            this.label25 = new System.Windows.Forms.Label();
            this.label26 = new System.Windows.Forms.Label();
            this.label27 = new System.Windows.Forms.Label();
            this.NomJoueurActuel = new System.Windows.Forms.Label();
            this.NomAvatarJoueurActuel = new System.Windows.Forms.Label();
            this.CarteJoueurActuel2 = new System.Windows.Forms.PictureBox();
            this.CarteJoueurActuel1 = new System.Windows.Forms.PictureBox();
            this.pictureBox7 = new System.Windows.Forms.PictureBox();
            this.richTextBox1 = new System.Windows.Forms.RichTextBox();
            this.GBJoueurVille = new System.Windows.Forms.GroupBox();
            this.lblNbPV = new System.Windows.Forms.Label();
            this.lblNbEne = new System.Windows.Forms.Label();
            this.lblNbVic = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.NomJoueurVille = new System.Windows.Forms.Label();
            this.NomAvatarJoueurVille = new System.Windows.Forms.Label();
            this.CarteJoueurVille3 = new System.Windows.Forms.PictureBox();
            this.CarteJoueurVille2 = new System.Windows.Forms.PictureBox();
            this.CarteJoueurVille1 = new System.Windows.Forms.PictureBox();
            this.pictBoxJoueurVile = new System.Windows.Forms.PictureBox();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.gbJForet3 = new System.Windows.Forms.GroupBox();
            this.EneJoueurForet3 = new System.Windows.Forms.Label();
            this.VicJoueurForet3 = new System.Windows.Forms.Label();
            this.VieJoueurForet3 = new System.Windows.Forms.Label();
            this.pictJoueurForet3 = new System.Windows.Forms.PictureBox();
            this.label16 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.NomJoueurForet3 = new System.Windows.Forms.Label();
            this.AvatarJoueurForet3 = new System.Windows.Forms.Label();
            this.gbJForet2 = new System.Windows.Forms.GroupBox();
            this.VicJoueurForet2 = new System.Windows.Forms.Label();
            this.EneJoueurForet2 = new System.Windows.Forms.Label();
            this.VieJoueurForet2 = new System.Windows.Forms.Label();
            this.pictJoueurForet2 = new System.Windows.Forms.PictureBox();
            this.label11 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.NomJoueurForet2 = new System.Windows.Forms.Label();
            this.AvatarJoueurForet2 = new System.Windows.Forms.Label();
            this.gbJForet1 = new System.Windows.Forms.GroupBox();
            this.EneJoueurForet1 = new System.Windows.Forms.Label();
            this.VicJoueurForet1 = new System.Windows.Forms.Label();
            this.VieJoueurForet1 = new System.Windows.Forms.Label();
            this.pictJoueurForet1 = new System.Windows.Forms.PictureBox();
            this.label10 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.NomJoueurForet1 = new System.Windows.Forms.Label();
            this.AvatarJoueurForet1 = new System.Windows.Forms.Label();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictRefillShop)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictShop3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictShop1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictShop2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictDe6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictDe5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictDe4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictDe3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictDe2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictDe1)).BeginInit();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.CarteJoueurActuel3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CarteJoueurActuel2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CarteJoueurActuel1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox7)).BeginInit();
            this.GBJoueurVille.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.CarteJoueurVille3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CarteJoueurVille2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CarteJoueurVille1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictBoxJoueurVile)).BeginInit();
            this.groupBox3.SuspendLayout();
            this.gbJForet3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictJoueurForet3)).BeginInit();
            this.gbJForet2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictJoueurForet2)).BeginInit();
            this.gbJForet1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictJoueurForet1)).BeginInit();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.checkBox9);
            this.groupBox1.Controls.Add(this.checkBox8);
            this.groupBox1.Controls.Add(this.checkBox7);
            this.groupBox1.Controls.Add(this.checkBox6);
            this.groupBox1.Controls.Add(this.checkBox5);
            this.groupBox1.Controls.Add(this.checkBox4);
            this.groupBox1.Controls.Add(this.checkBox3);
            this.groupBox1.Controls.Add(this.checkBox2);
            this.groupBox1.Controls.Add(this.checkBox1);
            this.groupBox1.Controls.Add(this.label22);
            this.groupBox1.Controls.Add(this.label21);
            this.groupBox1.Controls.Add(this.pictRefillShop);
            this.groupBox1.Controls.Add(this.pictShop3);
            this.groupBox1.Controls.Add(this.pictShop1);
            this.groupBox1.Controls.Add(this.pictShop2);
            this.groupBox1.Controls.Add(this.pictDe6);
            this.groupBox1.Controls.Add(this.pictDe5);
            this.groupBox1.Controls.Add(this.pictDe4);
            this.groupBox1.Controls.Add(this.pictDe3);
            this.groupBox1.Controls.Add(this.pictDe2);
            this.groupBox1.Controls.Add(this.pictDe1);
            this.groupBox1.Location = new System.Drawing.Point(608, 31);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(450, 275);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            // 
            // checkBox9
            // 
            this.checkBox9.AutoSize = true;
            this.checkBox9.Location = new System.Drawing.Point(194, 134);
            this.checkBox9.Name = "checkBox9";
            this.checkBox9.Size = new System.Drawing.Size(15, 14);
            this.checkBox9.TabIndex = 29;
            this.checkBox9.UseVisualStyleBackColor = true;
            // 
            // checkBox8
            // 
            this.checkBox8.AutoSize = true;
            this.checkBox8.Location = new System.Drawing.Point(124, 134);
            this.checkBox8.Name = "checkBox8";
            this.checkBox8.Size = new System.Drawing.Size(15, 14);
            this.checkBox8.TabIndex = 28;
            this.checkBox8.UseVisualStyleBackColor = true;
            // 
            // checkBox7
            // 
            this.checkBox7.AutoSize = true;
            this.checkBox7.Location = new System.Drawing.Point(50, 134);
            this.checkBox7.Name = "checkBox7";
            this.checkBox7.Size = new System.Drawing.Size(15, 14);
            this.checkBox7.TabIndex = 27;
            this.checkBox7.UseVisualStyleBackColor = true;
            // 
            // checkBox6
            // 
            this.checkBox6.AutoSize = true;
            this.checkBox6.Location = new System.Drawing.Point(322, 222);
            this.checkBox6.Name = "checkBox6";
            this.checkBox6.Size = new System.Drawing.Size(15, 14);
            this.checkBox6.TabIndex = 26;
            this.checkBox6.UseVisualStyleBackColor = true;
            // 
            // checkBox5
            // 
            this.checkBox5.AutoSize = true;
            this.checkBox5.Location = new System.Drawing.Point(266, 223);
            this.checkBox5.Name = "checkBox5";
            this.checkBox5.Size = new System.Drawing.Size(15, 14);
            this.checkBox5.TabIndex = 25;
            this.checkBox5.UseVisualStyleBackColor = true;
            // 
            // checkBox4
            // 
            this.checkBox4.AutoSize = true;
            this.checkBox4.Location = new System.Drawing.Point(203, 227);
            this.checkBox4.Name = "checkBox4";
            this.checkBox4.Size = new System.Drawing.Size(15, 14);
            this.checkBox4.TabIndex = 24;
            this.checkBox4.UseVisualStyleBackColor = true;
            // 
            // checkBox3
            // 
            this.checkBox3.AutoSize = true;
            this.checkBox3.Location = new System.Drawing.Point(154, 227);
            this.checkBox3.Name = "checkBox3";
            this.checkBox3.Size = new System.Drawing.Size(15, 14);
            this.checkBox3.TabIndex = 23;
            this.checkBox3.UseVisualStyleBackColor = true;
            // 
            // checkBox2
            // 
            this.checkBox2.AutoSize = true;
            this.checkBox2.Location = new System.Drawing.Point(98, 228);
            this.checkBox2.Name = "checkBox2";
            this.checkBox2.Size = new System.Drawing.Size(15, 14);
            this.checkBox2.TabIndex = 22;
            this.checkBox2.UseVisualStyleBackColor = true;
            // 
            // checkBox1
            // 
            this.checkBox1.AutoSize = true;
            this.checkBox1.Location = new System.Drawing.Point(36, 228);
            this.checkBox1.Name = "checkBox1";
            this.checkBox1.Size = new System.Drawing.Size(15, 14);
            this.checkBox1.TabIndex = 21;
            this.checkBox1.UseVisualStyleBackColor = true;
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Location = new System.Drawing.Point(310, 19);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(40, 13);
            this.label22.TabIndex = 20;
            this.label22.Text = "Pioche";
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Location = new System.Drawing.Point(85, 19);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(93, 13);
            this.label21.TabIndex = 19;
            this.label21.Text = "Cartes Achetables";
            // 
            // pictRefillShop
            // 
            this.pictRefillShop.Image = global::QOT_UserInterface.Properties.Resources.backCard;
            this.pictRefillShop.Location = new System.Drawing.Point(289, 41);
            this.pictRefillShop.Name = "pictRefillShop";
            this.pictRefillShop.Size = new System.Drawing.Size(83, 118);
            this.pictRefillShop.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictRefillShop.TabIndex = 10;
            this.pictRefillShop.TabStop = false;
            // 
            // pictShop3
            // 
            this.pictShop3.Image = global::QOT_UserInterface.Properties.Resources.backCard;
            this.pictShop3.Location = new System.Drawing.Point(178, 41);
            this.pictShop3.Name = "pictShop3";
            this.pictShop3.Size = new System.Drawing.Size(56, 87);
            this.pictShop3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictShop3.TabIndex = 8;
            this.pictShop3.TabStop = false;
            this.pictShop3.MouseHover += new System.EventHandler(this.pictShop3_MouseHover);
            // 
            // pictShop1
            // 
            this.pictShop1.Image = global::QOT_UserInterface.Properties.Resources.backCard;
            this.pictShop1.Location = new System.Drawing.Point(36, 41);
            this.pictShop1.Name = "pictShop1";
            this.pictShop1.Size = new System.Drawing.Size(55, 87);
            this.pictShop1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictShop1.TabIndex = 6;
            this.pictShop1.TabStop = false;
            this.pictShop1.MouseEnter += new System.EventHandler(this.pictShop1_MouseEnter);
            this.pictShop1.MouseLeave += new System.EventHandler(this.pictShop1_MouseLeave);
            this.pictShop1.MouseHover += new System.EventHandler(this.pictShop1_MouseHover);
            // 
            // pictShop2
            // 
            this.pictShop2.Image = global::QOT_UserInterface.Properties.Resources.backCard;
            this.pictShop2.Location = new System.Drawing.Point(104, 41);
            this.pictShop2.Name = "pictShop2";
            this.pictShop2.Size = new System.Drawing.Size(56, 87);
            this.pictShop2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictShop2.TabIndex = 7;
            this.pictShop2.TabStop = false;
            this.pictShop2.MouseHover += new System.EventHandler(this.pictShop2_MouseHover);
            // 
            // pictDe6
            // 
            this.pictDe6.Location = new System.Drawing.Point(313, 185);
            this.pictDe6.Name = "pictDe6";
            this.pictDe6.Size = new System.Drawing.Size(35, 36);
            this.pictDe6.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictDe6.TabIndex = 5;
            this.pictDe6.TabStop = false;
            // 
            // pictDe5
            // 
            this.pictDe5.Location = new System.Drawing.Point(255, 185);
            this.pictDe5.Name = "pictDe5";
            this.pictDe5.Size = new System.Drawing.Size(35, 36);
            this.pictDe5.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictDe5.TabIndex = 4;
            this.pictDe5.TabStop = false;
            // 
            // pictDe4
            // 
            this.pictDe4.Location = new System.Drawing.Point(199, 185);
            this.pictDe4.Name = "pictDe4";
            this.pictDe4.Size = new System.Drawing.Size(35, 36);
            this.pictDe4.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictDe4.TabIndex = 3;
            this.pictDe4.TabStop = false;
            // 
            // pictDe3
            // 
            this.pictDe3.Location = new System.Drawing.Point(143, 185);
            this.pictDe3.Name = "pictDe3";
            this.pictDe3.Size = new System.Drawing.Size(35, 36);
            this.pictDe3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictDe3.TabIndex = 2;
            this.pictDe3.TabStop = false;
            // 
            // pictDe2
            // 
            this.pictDe2.Location = new System.Drawing.Point(86, 185);
            this.pictDe2.Name = "pictDe2";
            this.pictDe2.Size = new System.Drawing.Size(35, 36);
            this.pictDe2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictDe2.TabIndex = 1;
            this.pictDe2.TabStop = false;
            // 
            // pictDe1
            // 
            this.pictDe1.Location = new System.Drawing.Point(30, 185);
            this.pictDe1.Name = "pictDe1";
            this.pictDe1.Size = new System.Drawing.Size(35, 36);
            this.pictDe1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictDe1.TabIndex = 0;
            this.pictDe1.TabStop = false;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.checkBox12);
            this.groupBox2.Controls.Add(this.checkBox11);
            this.groupBox2.Controls.Add(this.checkBox10);
            this.groupBox2.Controls.Add(this.btnFinTour);
            this.groupBox2.Controls.Add(this.btnLancer);
            this.groupBox2.Controls.Add(this.BtnAcheter);
            this.groupBox2.Controls.Add(this.btnVendre);
            this.groupBox2.Controls.Add(this.CarteJoueurActuel3);
            this.groupBox2.Controls.Add(this.lblNbPVJActuel);
            this.groupBox2.Controls.Add(this.lblNbEneJActuel);
            this.groupBox2.Controls.Add(this.lblNbVicJActuel);
            this.groupBox2.Controls.Add(this.label24);
            this.groupBox2.Controls.Add(this.label25);
            this.groupBox2.Controls.Add(this.label26);
            this.groupBox2.Controls.Add(this.label27);
            this.groupBox2.Controls.Add(this.NomJoueurActuel);
            this.groupBox2.Controls.Add(this.NomAvatarJoueurActuel);
            this.groupBox2.Controls.Add(this.CarteJoueurActuel2);
            this.groupBox2.Controls.Add(this.CarteJoueurActuel1);
            this.groupBox2.Controls.Add(this.pictureBox7);
            this.groupBox2.Location = new System.Drawing.Point(638, 322);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(420, 315);
            this.groupBox2.TabIndex = 1;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Joueur Actuel";
            // 
            // checkBox12
            // 
            this.checkBox12.AutoSize = true;
            this.checkBox12.Location = new System.Drawing.Point(279, 253);
            this.checkBox12.Name = "checkBox12";
            this.checkBox12.Size = new System.Drawing.Size(15, 14);
            this.checkBox12.TabIndex = 49;
            this.checkBox12.UseVisualStyleBackColor = true;
            // 
            // checkBox11
            // 
            this.checkBox11.AutoSize = true;
            this.checkBox11.Location = new System.Drawing.Point(220, 251);
            this.checkBox11.Name = "checkBox11";
            this.checkBox11.Size = new System.Drawing.Size(15, 14);
            this.checkBox11.TabIndex = 48;
            this.checkBox11.UseVisualStyleBackColor = true;
            // 
            // checkBox10
            // 
            this.checkBox10.AutoSize = true;
            this.checkBox10.Location = new System.Drawing.Point(164, 250);
            this.checkBox10.Name = "checkBox10";
            this.checkBox10.Size = new System.Drawing.Size(15, 14);
            this.checkBox10.TabIndex = 30;
            this.checkBox10.UseVisualStyleBackColor = true;
            // 
            // btnFinTour
            // 
            this.btnFinTour.Location = new System.Drawing.Point(290, 273);
            this.btnFinTour.Name = "btnFinTour";
            this.btnFinTour.Size = new System.Drawing.Size(124, 36);
            this.btnFinTour.TabIndex = 47;
            this.btnFinTour.Text = "Fin de tour";
            this.btnFinTour.UseVisualStyleBackColor = true;
            this.btnFinTour.Click += new System.EventHandler(this.btnFinTour_Click);
            // 
            // btnLancer
            // 
            this.btnLancer.Location = new System.Drawing.Point(25, 234);
            this.btnLancer.Name = "btnLancer";
            this.btnLancer.Size = new System.Drawing.Size(105, 47);
            this.btnLancer.TabIndex = 46;
            this.btnLancer.Text = "Lancer/Relancer";
            this.btnLancer.UseVisualStyleBackColor = true;
            this.btnLancer.Click += new System.EventHandler(this.btnLancer_Click);
            // 
            // BtnAcheter
            // 
            this.BtnAcheter.Location = new System.Drawing.Point(327, 209);
            this.BtnAcheter.Name = "BtnAcheter";
            this.BtnAcheter.Size = new System.Drawing.Size(87, 36);
            this.BtnAcheter.TabIndex = 45;
            this.BtnAcheter.Text = "Acheter Carte";
            this.BtnAcheter.UseVisualStyleBackColor = true;
            this.BtnAcheter.Click += new System.EventHandler(this.BtnAcheter_Click);
            // 
            // btnVendre
            // 
            this.btnVendre.Location = new System.Drawing.Point(327, 170);
            this.btnVendre.Name = "btnVendre";
            this.btnVendre.Size = new System.Drawing.Size(87, 36);
            this.btnVendre.TabIndex = 44;
            this.btnVendre.Text = "Vendre Carte";
            this.btnVendre.UseVisualStyleBackColor = true;
            this.btnVendre.Click += new System.EventHandler(this.btnVendre_Click);
            // 
            // CarteJoueurActuel3
            // 
            this.CarteJoueurActuel3.Image = global::QOT_UserInterface.Properties.Resources.backCard;
            this.CarteJoueurActuel3.Location = new System.Drawing.Point(260, 176);
            this.CarteJoueurActuel3.Name = "CarteJoueurActuel3";
            this.CarteJoueurActuel3.Size = new System.Drawing.Size(47, 71);
            this.CarteJoueurActuel3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.CarteJoueurActuel3.TabIndex = 43;
            this.CarteJoueurActuel3.TabStop = false;
            this.CarteJoueurActuel3.Click += new System.EventHandler(this.CarteJoueurActuel3_Click);
            this.CarteJoueurActuel3.MouseHover += new System.EventHandler(this.CarteJoueurActuel3_MouseHover);
            // 
            // lblNbPVJActuel
            // 
            this.lblNbPVJActuel.AutoSize = true;
            this.lblNbPVJActuel.Location = new System.Drawing.Point(233, 101);
            this.lblNbPVJActuel.Name = "lblNbPVJActuel";
            this.lblNbPVJActuel.Size = new System.Drawing.Size(104, 13);
            this.lblNbPVJActuel.TabIndex = 42;
            this.lblNbPVJActuel.Text = "<<lblNbPVJActuel>>";
            // 
            // lblNbEneJActuel
            // 
            this.lblNbEneJActuel.AutoSize = true;
            this.lblNbEneJActuel.Location = new System.Drawing.Point(233, 145);
            this.lblNbEneJActuel.Name = "lblNbEneJActuel";
            this.lblNbEneJActuel.Size = new System.Drawing.Size(109, 13);
            this.lblNbEneJActuel.TabIndex = 41;
            this.lblNbEneJActuel.Text = "<<lblNbEneJActuel>>";
            // 
            // lblNbVicJActuel
            // 
            this.lblNbVicJActuel.AutoSize = true;
            this.lblNbVicJActuel.Location = new System.Drawing.Point(233, 123);
            this.lblNbVicJActuel.Name = "lblNbVicJActuel";
            this.lblNbVicJActuel.Size = new System.Drawing.Size(105, 13);
            this.lblNbVicJActuel.TabIndex = 40;
            this.lblNbVicJActuel.Text = "<<lblNbVicJActuel>>";
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Location = new System.Drawing.Point(186, 145);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(49, 13);
            this.label24.TabIndex = 39;
            this.label24.Text = "Energie :";
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Location = new System.Drawing.Point(141, 123);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(94, 13);
            this.label25.TabIndex = 38;
            this.label25.Text = "Points de victoire :";
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Location = new System.Drawing.Point(161, 101);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(74, 13);
            this.label26.TabIndex = 37;
            this.label26.Text = "Points de vie :";
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Location = new System.Drawing.Point(141, 78);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(67, 13);
            this.label27.TabIndex = 36;
            this.label27.Text = "Statistiques :";
            // 
            // NomJoueurActuel
            // 
            this.NomJoueurActuel.AutoSize = true;
            this.NomJoueurActuel.Location = new System.Drawing.Point(141, 49);
            this.NomJoueurActuel.Name = "NomJoueurActuel";
            this.NomJoueurActuel.Size = new System.Drawing.Size(115, 13);
            this.NomJoueurActuel.TabIndex = 35;
            this.NomJoueurActuel.Text = "<<NomJoueurActuel>>";
            // 
            // NomAvatarJoueurActuel
            // 
            this.NomAvatarJoueurActuel.AutoSize = true;
            this.NomAvatarJoueurActuel.Location = new System.Drawing.Point(141, 27);
            this.NomAvatarJoueurActuel.Name = "NomAvatarJoueurActuel";
            this.NomAvatarJoueurActuel.Size = new System.Drawing.Size(146, 13);
            this.NomAvatarJoueurActuel.TabIndex = 34;
            this.NomAvatarJoueurActuel.Text = "<<NomAvatarJoueurActuel>>";
            // 
            // CarteJoueurActuel2
            // 
            this.CarteJoueurActuel2.Image = global::QOT_UserInterface.Properties.Resources.backCard;
            this.CarteJoueurActuel2.Location = new System.Drawing.Point(204, 176);
            this.CarteJoueurActuel2.Name = "CarteJoueurActuel2";
            this.CarteJoueurActuel2.Size = new System.Drawing.Size(47, 71);
            this.CarteJoueurActuel2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.CarteJoueurActuel2.TabIndex = 30;
            this.CarteJoueurActuel2.TabStop = false;
            this.CarteJoueurActuel2.Click += new System.EventHandler(this.CarteJoueurActuel2_Click);
            this.CarteJoueurActuel2.MouseHover += new System.EventHandler(this.CarteJoueurActuel2_MouseHover);
            // 
            // CarteJoueurActuel1
            // 
            this.CarteJoueurActuel1.Image = global::QOT_UserInterface.Properties.Resources.backCard;
            this.CarteJoueurActuel1.Location = new System.Drawing.Point(151, 176);
            this.CarteJoueurActuel1.Name = "CarteJoueurActuel1";
            this.CarteJoueurActuel1.Size = new System.Drawing.Size(47, 71);
            this.CarteJoueurActuel1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.CarteJoueurActuel1.TabIndex = 29;
            this.CarteJoueurActuel1.TabStop = false;
            this.CarteJoueurActuel1.Click += new System.EventHandler(this.CarteJoueurActuel1_Click);
            this.CarteJoueurActuel1.MouseHover += new System.EventHandler(this.CarteJoueurActuel1_MouseHover);
            // 
            // pictureBox7
            // 
            this.pictureBox7.Location = new System.Drawing.Point(6, 19);
            this.pictureBox7.Name = "pictureBox7";
            this.pictureBox7.Size = new System.Drawing.Size(129, 187);
            this.pictureBox7.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox7.TabIndex = 27;
            this.pictureBox7.TabStop = false;
            // 
            // richTextBox1
            // 
            this.richTextBox1.Location = new System.Drawing.Point(1064, 31);
            this.richTextBox1.Name = "richTextBox1";
            this.richTextBox1.ReadOnly = true;
            this.richTextBox1.Size = new System.Drawing.Size(244, 606);
            this.richTextBox1.TabIndex = 2;
            this.richTextBox1.Text = "";
            // 
            // GBJoueurVille
            // 
            this.GBJoueurVille.BackColor = System.Drawing.Color.Transparent;
            this.GBJoueurVille.BackgroundImage = global::QOT_UserInterface.Properties.Resources.ville_nocturne;
            this.GBJoueurVille.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.GBJoueurVille.Controls.Add(this.lblNbPV);
            this.GBJoueurVille.Controls.Add(this.lblNbEne);
            this.GBJoueurVille.Controls.Add(this.lblNbVic);
            this.GBJoueurVille.Controls.Add(this.label5);
            this.GBJoueurVille.Controls.Add(this.label4);
            this.GBJoueurVille.Controls.Add(this.label3);
            this.GBJoueurVille.Controls.Add(this.label2);
            this.GBJoueurVille.Controls.Add(this.NomJoueurVille);
            this.GBJoueurVille.Controls.Add(this.NomAvatarJoueurVille);
            this.GBJoueurVille.Controls.Add(this.CarteJoueurVille3);
            this.GBJoueurVille.Controls.Add(this.CarteJoueurVille2);
            this.GBJoueurVille.Controls.Add(this.CarteJoueurVille1);
            this.GBJoueurVille.Controls.Add(this.pictBoxJoueurVile);
            this.GBJoueurVille.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.GBJoueurVille.Location = new System.Drawing.Point(12, 31);
            this.GBJoueurVille.Name = "GBJoueurVille";
            this.GBJoueurVille.Size = new System.Drawing.Size(590, 275);
            this.GBJoueurVille.TabIndex = 1;
            this.GBJoueurVille.TabStop = false;
            this.GBJoueurVille.Text = "Joueur dans la ville";
            // 
            // lblNbPV
            // 
            this.lblNbPV.AutoSize = true;
            this.lblNbPV.Location = new System.Drawing.Point(258, 102);
            this.lblNbPV.Name = "lblNbPV";
            this.lblNbPV.Size = new System.Drawing.Size(69, 13);
            this.lblNbPV.TabIndex = 26;
            this.lblNbPV.Text = "<<lblNbPV>>";
            // 
            // lblNbEne
            // 
            this.lblNbEne.AutoSize = true;
            this.lblNbEne.Location = new System.Drawing.Point(258, 146);
            this.lblNbEne.Name = "lblNbEne";
            this.lblNbEne.Size = new System.Drawing.Size(74, 13);
            this.lblNbEne.TabIndex = 25;
            this.lblNbEne.Text = "<<lblNbEne>>";
            // 
            // lblNbVic
            // 
            this.lblNbVic.AutoSize = true;
            this.lblNbVic.Location = new System.Drawing.Point(258, 124);
            this.lblNbVic.Name = "lblNbVic";
            this.lblNbVic.Size = new System.Drawing.Size(70, 13);
            this.lblNbVic.TabIndex = 24;
            this.lblNbVic.Text = "<<lblNbVic>>";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(211, 146);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(49, 13);
            this.label5.TabIndex = 23;
            this.label5.Text = "Energie :";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(166, 124);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(94, 13);
            this.label4.TabIndex = 22;
            this.label4.Text = "Points de victoire :";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(186, 102);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(74, 13);
            this.label3.TabIndex = 21;
            this.label3.Text = "Points de vie :";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(166, 79);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(67, 13);
            this.label2.TabIndex = 20;
            this.label2.Text = "Statistiques :";
            // 
            // NomJoueurVille
            // 
            this.NomJoueurVille.AutoSize = true;
            this.NomJoueurVille.Location = new System.Drawing.Point(166, 50);
            this.NomJoueurVille.Name = "NomJoueurVille";
            this.NomJoueurVille.Size = new System.Drawing.Size(104, 13);
            this.NomJoueurVille.TabIndex = 19;
            this.NomJoueurVille.Text = "<<NomJoueurVille>>";
            // 
            // NomAvatarJoueurVille
            // 
            this.NomAvatarJoueurVille.AutoSize = true;
            this.NomAvatarJoueurVille.Location = new System.Drawing.Point(166, 28);
            this.NomAvatarJoueurVille.Name = "NomAvatarJoueurVille";
            this.NomAvatarJoueurVille.Size = new System.Drawing.Size(135, 13);
            this.NomAvatarJoueurVille.TabIndex = 18;
            this.NomAvatarJoueurVille.Text = "<<NomAvatarJoueurVille>>";
            // 
            // CarteJoueurVille3
            // 
            this.CarteJoueurVille3.Image = global::QOT_UserInterface.Properties.Resources.backCard;
            this.CarteJoueurVille3.Location = new System.Drawing.Point(509, 146);
            this.CarteJoueurVille3.Name = "CarteJoueurVille3";
            this.CarteJoueurVille3.Size = new System.Drawing.Size(75, 114);
            this.CarteJoueurVille3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.CarteJoueurVille3.TabIndex = 15;
            this.CarteJoueurVille3.TabStop = false;
            this.CarteJoueurVille3.MouseHover += new System.EventHandler(this.CarteJoueurVille3_MouseHover);
            // 
            // CarteJoueurVille2
            // 
            this.CarteJoueurVille2.Image = global::QOT_UserInterface.Properties.Resources.backCard;
            this.CarteJoueurVille2.Location = new System.Drawing.Point(428, 146);
            this.CarteJoueurVille2.Name = "CarteJoueurVille2";
            this.CarteJoueurVille2.Size = new System.Drawing.Size(75, 114);
            this.CarteJoueurVille2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.CarteJoueurVille2.TabIndex = 14;
            this.CarteJoueurVille2.TabStop = false;
            this.CarteJoueurVille2.MouseHover += new System.EventHandler(this.CarteJoueurVille2_MouseHover);
            // 
            // CarteJoueurVille1
            // 
            this.CarteJoueurVille1.Image = global::QOT_UserInterface.Properties.Resources.backCard;
            this.CarteJoueurVille1.Location = new System.Drawing.Point(347, 146);
            this.CarteJoueurVille1.Name = "CarteJoueurVille1";
            this.CarteJoueurVille1.Size = new System.Drawing.Size(75, 114);
            this.CarteJoueurVille1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.CarteJoueurVille1.TabIndex = 13;
            this.CarteJoueurVille1.TabStop = false;
            this.CarteJoueurVille1.MouseHover += new System.EventHandler(this.CarteJoueurVille1_MouseHover);
            // 
            // pictBoxJoueurVile
            // 
            this.pictBoxJoueurVile.Location = new System.Drawing.Point(20, 19);
            this.pictBoxJoueurVile.Name = "pictBoxJoueurVile";
            this.pictBoxJoueurVile.Size = new System.Drawing.Size(140, 241);
            this.pictBoxJoueurVile.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictBoxJoueurVile.TabIndex = 11;
            this.pictBoxJoueurVile.TabStop = false;
            // 
            // groupBox3
            // 
            this.groupBox3.BackgroundImage = global::QOT_UserInterface.Properties.Resources.foret_nocturne;
            this.groupBox3.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.groupBox3.Controls.Add(this.gbJForet3);
            this.groupBox3.Controls.Add(this.gbJForet2);
            this.groupBox3.Controls.Add(this.gbJForet1);
            this.groupBox3.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.groupBox3.Location = new System.Drawing.Point(12, 322);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(609, 315);
            this.groupBox3.TabIndex = 1;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Joueurs dans la forêt";
            // 
            // gbJForet3
            // 
            this.gbJForet3.BackColor = System.Drawing.Color.Transparent;
            this.gbJForet3.Controls.Add(this.EneJoueurForet3);
            this.gbJForet3.Controls.Add(this.VicJoueurForet3);
            this.gbJForet3.Controls.Add(this.VieJoueurForet3);
            this.gbJForet3.Controls.Add(this.pictJoueurForet3);
            this.gbJForet3.Controls.Add(this.label16);
            this.gbJForet3.Controls.Add(this.label17);
            this.gbJForet3.Controls.Add(this.label18);
            this.gbJForet3.Controls.Add(this.NomJoueurForet3);
            this.gbJForet3.Controls.Add(this.AvatarJoueurForet3);
            this.gbJForet3.Location = new System.Drawing.Point(425, 16);
            this.gbJForet3.Name = "gbJForet3";
            this.gbJForet3.Size = new System.Drawing.Size(178, 290);
            this.gbJForet3.TabIndex = 29;
            this.gbJForet3.TabStop = false;
            this.gbJForet3.Visible = false;
            // 
            // EneJoueurForet3
            // 
            this.EneJoueurForet3.AutoSize = true;
            this.EneJoueurForet3.Location = new System.Drawing.Point(91, 259);
            this.EneJoueurForet3.Name = "EneJoueurForet3";
            this.EneJoueurForet3.Size = new System.Drawing.Size(112, 13);
            this.EneJoueurForet3.TabIndex = 26;
            this.EneJoueurForet3.Text = "<<EneJoueurForet3>>";
            // 
            // VicJoueurForet3
            // 
            this.VicJoueurForet3.AutoSize = true;
            this.VicJoueurForet3.Location = new System.Drawing.Point(91, 235);
            this.VicJoueurForet3.Name = "VicJoueurForet3";
            this.VicJoueurForet3.Size = new System.Drawing.Size(108, 13);
            this.VicJoueurForet3.TabIndex = 25;
            this.VicJoueurForet3.Text = "<<VicJoueurForet3>>";
            // 
            // VieJoueurForet3
            // 
            this.VieJoueurForet3.AutoSize = true;
            this.VieJoueurForet3.Location = new System.Drawing.Point(91, 211);
            this.VieJoueurForet3.Name = "VieJoueurForet3";
            this.VieJoueurForet3.Size = new System.Drawing.Size(108, 13);
            this.VieJoueurForet3.TabIndex = 24;
            this.VieJoueurForet3.Text = "<<VieJoueurForet3>>";
            // 
            // pictJoueurForet3
            // 
            this.pictJoueurForet3.Location = new System.Drawing.Point(18, 60);
            this.pictJoueurForet3.Name = "pictJoueurForet3";
            this.pictJoueurForet3.Size = new System.Drawing.Size(119, 148);
            this.pictJoueurForet3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictJoueurForet3.TabIndex = 17;
            this.pictJoueurForet3.TabStop = false;
            this.pictJoueurForet3.Click += new System.EventHandler(this.pictJoueurForet3_Click);
            this.pictJoueurForet3.MouseHover += new System.EventHandler(this.pictJoueurForet3_MouseHover);
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(36, 259);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(49, 13);
            this.label16.TabIndex = 16;
            this.label16.Text = "Energie :";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(36, 235);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(48, 13);
            this.label17.TabIndex = 15;
            this.label17.Text = "Victoire :";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(57, 211);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(28, 13);
            this.label18.TabIndex = 14;
            this.label18.Text = "Vie :";
            // 
            // NomJoueurForet3
            // 
            this.NomJoueurForet3.AutoSize = true;
            this.NomJoueurForet3.Location = new System.Drawing.Point(15, 34);
            this.NomJoueurForet3.Name = "NomJoueurForet3";
            this.NomJoueurForet3.Size = new System.Drawing.Size(115, 13);
            this.NomJoueurForet3.TabIndex = 13;
            this.NomJoueurForet3.Text = "<<NomJoueurForet3>>";
            // 
            // AvatarJoueurForet3
            // 
            this.AvatarJoueurForet3.AutoSize = true;
            this.AvatarJoueurForet3.Location = new System.Drawing.Point(15, 11);
            this.AvatarJoueurForet3.Name = "AvatarJoueurForet3";
            this.AvatarJoueurForet3.Size = new System.Drawing.Size(124, 13);
            this.AvatarJoueurForet3.TabIndex = 12;
            this.AvatarJoueurForet3.Text = "<<AvatarJoueurForet3>>";
            // 
            // gbJForet2
            // 
            this.gbJForet2.BackColor = System.Drawing.Color.Transparent;
            this.gbJForet2.Controls.Add(this.VicJoueurForet2);
            this.gbJForet2.Controls.Add(this.EneJoueurForet2);
            this.gbJForet2.Controls.Add(this.VieJoueurForet2);
            this.gbJForet2.Controls.Add(this.pictJoueurForet2);
            this.gbJForet2.Controls.Add(this.label11);
            this.gbJForet2.Controls.Add(this.label12);
            this.gbJForet2.Controls.Add(this.label13);
            this.gbJForet2.Controls.Add(this.NomJoueurForet2);
            this.gbJForet2.Controls.Add(this.AvatarJoueurForet2);
            this.gbJForet2.Location = new System.Drawing.Point(221, 16);
            this.gbJForet2.Name = "gbJForet2";
            this.gbJForet2.Size = new System.Drawing.Size(178, 289);
            this.gbJForet2.TabIndex = 28;
            this.gbJForet2.TabStop = false;
            this.gbJForet2.Visible = false;
            // 
            // VicJoueurForet2
            // 
            this.VicJoueurForet2.AutoSize = true;
            this.VicJoueurForet2.Location = new System.Drawing.Point(91, 235);
            this.VicJoueurForet2.Name = "VicJoueurForet2";
            this.VicJoueurForet2.Size = new System.Drawing.Size(108, 13);
            this.VicJoueurForet2.TabIndex = 23;
            this.VicJoueurForet2.Text = "<<VicJoueurForet2>>";
            // 
            // EneJoueurForet2
            // 
            this.EneJoueurForet2.AutoSize = true;
            this.EneJoueurForet2.Location = new System.Drawing.Point(91, 259);
            this.EneJoueurForet2.Name = "EneJoueurForet2";
            this.EneJoueurForet2.Size = new System.Drawing.Size(112, 13);
            this.EneJoueurForet2.TabIndex = 22;
            this.EneJoueurForet2.Text = "<<EneJoueurForet2>>";
            // 
            // VieJoueurForet2
            // 
            this.VieJoueurForet2.AutoSize = true;
            this.VieJoueurForet2.Location = new System.Drawing.Point(91, 211);
            this.VieJoueurForet2.Name = "VieJoueurForet2";
            this.VieJoueurForet2.Size = new System.Drawing.Size(108, 13);
            this.VieJoueurForet2.TabIndex = 21;
            this.VieJoueurForet2.Text = "<<VieJoueurForet2>>";
            // 
            // pictJoueurForet2
            // 
            this.pictJoueurForet2.Location = new System.Drawing.Point(19, 60);
            this.pictJoueurForet2.Name = "pictJoueurForet2";
            this.pictJoueurForet2.Size = new System.Drawing.Size(119, 148);
            this.pictJoueurForet2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictJoueurForet2.TabIndex = 11;
            this.pictJoueurForet2.TabStop = false;
            this.pictJoueurForet2.MouseHover += new System.EventHandler(this.pictJoueurForet2_MouseHover);
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(37, 259);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(49, 13);
            this.label11.TabIndex = 10;
            this.label11.Text = "Energie :";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(37, 235);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(48, 13);
            this.label12.TabIndex = 9;
            this.label12.Text = "Victoire :";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(58, 211);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(28, 13);
            this.label13.TabIndex = 8;
            this.label13.Text = "Vie :";
            // 
            // NomJoueurForet2
            // 
            this.NomJoueurForet2.AutoSize = true;
            this.NomJoueurForet2.Location = new System.Drawing.Point(16, 34);
            this.NomJoueurForet2.Name = "NomJoueurForet2";
            this.NomJoueurForet2.Size = new System.Drawing.Size(115, 13);
            this.NomJoueurForet2.TabIndex = 7;
            this.NomJoueurForet2.Text = "<<NomJoueurForet2>>";
            // 
            // AvatarJoueurForet2
            // 
            this.AvatarJoueurForet2.AutoSize = true;
            this.AvatarJoueurForet2.Location = new System.Drawing.Point(16, 11);
            this.AvatarJoueurForet2.Name = "AvatarJoueurForet2";
            this.AvatarJoueurForet2.Size = new System.Drawing.Size(124, 13);
            this.AvatarJoueurForet2.TabIndex = 6;
            this.AvatarJoueurForet2.Text = "<<AvatarJoueurForet2>>";
            // 
            // gbJForet1
            // 
            this.gbJForet1.BackColor = System.Drawing.Color.Transparent;
            this.gbJForet1.Controls.Add(this.EneJoueurForet1);
            this.gbJForet1.Controls.Add(this.VicJoueurForet1);
            this.gbJForet1.Controls.Add(this.VieJoueurForet1);
            this.gbJForet1.Controls.Add(this.pictJoueurForet1);
            this.gbJForet1.Controls.Add(this.label10);
            this.gbJForet1.Controls.Add(this.label9);
            this.gbJForet1.Controls.Add(this.label8);
            this.gbJForet1.Controls.Add(this.NomJoueurForet1);
            this.gbJForet1.Controls.Add(this.AvatarJoueurForet1);
            this.gbJForet1.Location = new System.Drawing.Point(10, 18);
            this.gbJForet1.Name = "gbJForet1";
            this.gbJForet1.Size = new System.Drawing.Size(178, 286);
            this.gbJForet1.TabIndex = 27;
            this.gbJForet1.TabStop = false;
            this.gbJForet1.Visible = false;
            // 
            // EneJoueurForet1
            // 
            this.EneJoueurForet1.AutoSize = true;
            this.EneJoueurForet1.Location = new System.Drawing.Point(93, 262);
            this.EneJoueurForet1.Name = "EneJoueurForet1";
            this.EneJoueurForet1.Size = new System.Drawing.Size(112, 13);
            this.EneJoueurForet1.TabIndex = 20;
            this.EneJoueurForet1.Text = "<<EneJoueurForet1>>";
            // 
            // VicJoueurForet1
            // 
            this.VicJoueurForet1.AutoSize = true;
            this.VicJoueurForet1.Location = new System.Drawing.Point(92, 238);
            this.VicJoueurForet1.Name = "VicJoueurForet1";
            this.VicJoueurForet1.Size = new System.Drawing.Size(108, 13);
            this.VicJoueurForet1.TabIndex = 19;
            this.VicJoueurForet1.Text = "<<VicJoueurForet1>>";
            // 
            // VieJoueurForet1
            // 
            this.VieJoueurForet1.AutoSize = true;
            this.VieJoueurForet1.Location = new System.Drawing.Point(92, 216);
            this.VieJoueurForet1.Name = "VieJoueurForet1";
            this.VieJoueurForet1.Size = new System.Drawing.Size(108, 13);
            this.VieJoueurForet1.TabIndex = 18;
            this.VieJoueurForet1.Text = "<<VieJoueurForet1>>";
            // 
            // pictJoueurForet1
            // 
            this.pictJoueurForet1.Location = new System.Drawing.Point(20, 63);
            this.pictJoueurForet1.Name = "pictJoueurForet1";
            this.pictJoueurForet1.Size = new System.Drawing.Size(119, 148);
            this.pictJoueurForet1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictJoueurForet1.TabIndex = 5;
            this.pictJoueurForet1.TabStop = false;
            this.pictJoueurForet1.MouseHover += new System.EventHandler(this.pictJoueurForet1_MouseHover);
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(38, 262);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(49, 13);
            this.label10.TabIndex = 4;
            this.label10.Text = "Energie :";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(38, 238);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(48, 13);
            this.label9.TabIndex = 3;
            this.label9.Text = "Victoire :";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(58, 216);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(28, 13);
            this.label8.TabIndex = 2;
            this.label8.Text = "Vie :";
            // 
            // NomJoueurForet1
            // 
            this.NomJoueurForet1.AutoSize = true;
            this.NomJoueurForet1.Location = new System.Drawing.Point(17, 32);
            this.NomJoueurForet1.Name = "NomJoueurForet1";
            this.NomJoueurForet1.Size = new System.Drawing.Size(115, 13);
            this.NomJoueurForet1.TabIndex = 1;
            this.NomJoueurForet1.Text = "<<NomJoueurForet1>>";
            // 
            // AvatarJoueurForet1
            // 
            this.AvatarJoueurForet1.AutoSize = true;
            this.AvatarJoueurForet1.Location = new System.Drawing.Point(17, 9);
            this.AvatarJoueurForet1.Name = "AvatarJoueurForet1";
            this.AvatarJoueurForet1.Size = new System.Drawing.Size(124, 13);
            this.AvatarJoueurForet1.TabIndex = 0;
            this.AvatarJoueurForet1.Text = "<<AvatarJoueurForet1>>";
            // 
            // Plateau
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.ClientSize = new System.Drawing.Size(1320, 659);
            this.Controls.Add(this.richTextBox1);
            this.Controls.Add(this.GBJoueurVille);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Name = "Plateau";
            this.Text = "Plateau";
            this.Load += new System.EventHandler(this.Plateau_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictRefillShop)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictShop3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictShop1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictShop2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictDe6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictDe5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictDe4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictDe3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictDe2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictDe1)).EndInit();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.CarteJoueurActuel3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CarteJoueurActuel2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CarteJoueurActuel1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox7)).EndInit();
            this.GBJoueurVille.ResumeLayout(false);
            this.GBJoueurVille.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.CarteJoueurVille3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CarteJoueurVille2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CarteJoueurVille1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictBoxJoueurVile)).EndInit();
            this.groupBox3.ResumeLayout(false);
            this.gbJForet3.ResumeLayout(false);
            this.gbJForet3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictJoueurForet3)).EndInit();
            this.gbJForet2.ResumeLayout(false);
            this.gbJForet2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictJoueurForet2)).EndInit();
            this.gbJForet1.ResumeLayout(false);
            this.gbJForet1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictJoueurForet1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.PictureBox pictRefillShop;
        private System.Windows.Forms.PictureBox pictShop3;
        private System.Windows.Forms.PictureBox pictShop2;
        private System.Windows.Forms.PictureBox pictShop1;
        private System.Windows.Forms.PictureBox pictDe6;
        private System.Windows.Forms.PictureBox pictDe5;
        private System.Windows.Forms.PictureBox pictDe4;
        private System.Windows.Forms.PictureBox pictDe3;
        private System.Windows.Forms.PictureBox pictDe2;
        private System.Windows.Forms.PictureBox pictDe1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.GroupBox GBJoueurVille;
        private System.Windows.Forms.Label NomJoueurVille;
        private System.Windows.Forms.Label NomAvatarJoueurVille;
        private System.Windows.Forms.PictureBox CarteJoueurVille3;
        private System.Windows.Forms.PictureBox CarteJoueurVille2;
        private System.Windows.Forms.PictureBox CarteJoueurVille1;
        private System.Windows.Forms.PictureBox pictBoxJoueurVile;
        private System.Windows.Forms.Label lblNbPV;
        private System.Windows.Forms.Label lblNbEne;
        private System.Windows.Forms.Label lblNbVic;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button btnFinTour;
        private System.Windows.Forms.Button btnLancer;
        private System.Windows.Forms.Button BtnAcheter;
        private System.Windows.Forms.Button btnVendre;
        private System.Windows.Forms.PictureBox CarteJoueurActuel3;
        private System.Windows.Forms.Label lblNbPVJActuel;
        private System.Windows.Forms.Label lblNbEneJActuel;
        private System.Windows.Forms.Label lblNbVicJActuel;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.Label NomJoueurActuel;
        private System.Windows.Forms.Label NomAvatarJoueurActuel;
        private System.Windows.Forms.PictureBox CarteJoueurActuel2;
        private System.Windows.Forms.PictureBox CarteJoueurActuel1;
        private System.Windows.Forms.PictureBox pictureBox7;
        private System.Windows.Forms.PictureBox pictJoueurForet3;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label NomJoueurForet3;
        private System.Windows.Forms.Label AvatarJoueurForet3;
        private System.Windows.Forms.PictureBox pictJoueurForet2;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label NomJoueurForet2;
        private System.Windows.Forms.Label AvatarJoueurForet2;
        private System.Windows.Forms.PictureBox pictJoueurForet1;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label NomJoueurForet1;
        private System.Windows.Forms.Label AvatarJoueurForet1;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Label EneJoueurForet3;
        private System.Windows.Forms.Label VicJoueurForet3;
        private System.Windows.Forms.Label VieJoueurForet3;
        private System.Windows.Forms.Label VicJoueurForet2;
        private System.Windows.Forms.Label EneJoueurForet2;
        private System.Windows.Forms.Label VieJoueurForet2;
        private System.Windows.Forms.Label EneJoueurForet1;
        private System.Windows.Forms.Label VicJoueurForet1;
        private System.Windows.Forms.Label VieJoueurForet1;
        private System.Windows.Forms.GroupBox gbJForet3;
        private System.Windows.Forms.GroupBox gbJForet2;
        private System.Windows.Forms.GroupBox gbJForet1;
        private System.Windows.Forms.CheckBox checkBox6;
        private System.Windows.Forms.CheckBox checkBox5;
        private System.Windows.Forms.CheckBox checkBox4;
        private System.Windows.Forms.CheckBox checkBox3;
        private System.Windows.Forms.CheckBox checkBox2;
        private System.Windows.Forms.CheckBox checkBox1;
        private System.Windows.Forms.CheckBox checkBox9;
        private System.Windows.Forms.CheckBox checkBox8;
        private System.Windows.Forms.CheckBox checkBox7;
        private System.Windows.Forms.CheckBox checkBox12;
        private System.Windows.Forms.CheckBox checkBox11;
        private System.Windows.Forms.CheckBox checkBox10;
        private System.Windows.Forms.RichTextBox richTextBox1;
    }
}
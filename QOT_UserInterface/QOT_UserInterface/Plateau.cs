﻿using QOT_Application;
using QOT_Domain_Carte;
using QOT_Domain_GameMaster;
using QOT_Domain_Joueur;
using QueenOfTokyo_Infrastructure;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace QOT_UserInterface
{
    public partial class Plateau : Form
    {
        private DebutPartie _DebutPartie;
        private int nbJoueurs;
        private InfosCartes infosCarte;
        private List<Joueur> listJoueurs;
        private Joueur joueurLocal;

        public Plateau(DebutPartie debPartie)
        {
            InitializeComponent();
            this._DebutPartie = debPartie;
            this.nbJoueurs = this._DebutPartie.GetNbJoueurs();
            this.listJoueurs = this._DebutPartie.listJoueur;
            this.joueurLocal = this._DebutPartie.joueurLocal;

            switch (nbJoueurs)
            {
                case 2:
                    this.gbJForet1.Visible = true;
                    break;

                case 3:
                    this.gbJForet1.Visible = true;
                    this.gbJForet2.Visible = true;
                    break;

                case 4:
                    this.gbJForet1.Visible = true;
                    this.gbJForet2.Visible = true;
                    this.gbJForet3.Visible = true;
                    break;

            }
        }


        private void Plateau_Load(object sender, EventArgs e)
        {

            Application.ApplicationExit += new EventHandler(Service_Infrastructure.ClearConnexion);

            PhaseJeu.InitCartes();

            Task.Factory.StartNew(() => GameMaster.BoucleGameplay(listJoueurs, joueurLocal));


            Thread tUpdateIHM = new Thread(new ThreadStart(UpdateIHM));
            tUpdateIHM.Start();

            //Thread tServer = new Thread(new ThreadStart(PhaseJeu.LaunchGameplay));
            //tServer.Start();
            PhaseJeu.LaunchGameplay();


            // GameMaster.BoucleGameplay(listJoueurs);
        }


        private void ChangeLabelInThread(Label lbl, string text)
        {
            MethodInvoker invoker = delegate
            {
                lbl.Text = text;
                lbl.Visible = true;
            };
            this.Invoke(invoker);
        }

        private void ChangeButtonInThread(Button btn, bool toEnableOrNot)
        {
            MethodInvoker invoker = delegate
            {
                btn.Enabled = toEnableOrNot;
            };
            this.Invoke(invoker);
        }

        private void ChangeRichTextBoxInThread(RichTextBox rtb, string text)
        {
            MethodInvoker invoker = delegate
            {
                if (text != string.Empty)
                {
                    rtb.AppendText(text);
                    rtb.SelectionStart = rtb.Text.Length;
                    rtb.ScrollToCaret();
                }
            };
            this.Invoke(invoker);
        }

        private Bitmap WhichAvatarIHM(Joueur j)
        {
            if (j.NomAvatar == "Yuno")
                return Properties.Resources.Yuno;
            if (j.NomAvatar == "ZeroTwo")
                return Properties.Resources.ZeroTwo;
            if (j.NomAvatar == "2B")
                return Properties.Resources._2B;
            if (j.NomAvatar == "Rem")
                return Properties.Resources.Rem;
            if (j.NomAvatar == "Nezuko")
                return Properties.Resources.Nezuko;
            else
                throw new Exception();
        }

        private void ChangePictureBoxAvatarInThread(PictureBox pctB, Joueur j)
        {
            MethodInvoker invoker = delegate
            {
                pctB.Image = WhichAvatarIHM(j);
            };
            this.Invoke(invoker);
        }

        private void ChangePictureBoxCarteInThread(PictureBox pctB, Carte c)
        {
            MethodInvoker invoker = delegate
            {
                if (c == null)
                {
                    pctB.Image = Properties.Resources.backCard;
                }
                else
                {
                    pctB.Image = WhichCarteIHM(c);
                }
            };
            this.Invoke(invoker);
        }

        private Bitmap WhichDesIHM(Des d)
        {
            if (d.Valeur == "Un")
                return Properties.Resources.de1;
            if (d.Valeur == "Deux")
                return Properties.Resources.de2;
            if (d.Valeur == "Trois")
                return Properties.Resources.de3;
            if (d.Valeur == "Baffe")
                return Properties.Resources.deB;
            if (d.Valeur == "Energie")
                return Properties.Resources.deE;
            if (d.Valeur == "Coeur")
                return Properties.Resources.deC;
            else
                throw new Exception();
        }

        private Bitmap WhichCarteIHM(Carte c)
        {
            if (c.Nom == "Soin")
                return Properties.Resources.Soin;
            if (c.Nom == "Tramway")
                return Properties.Resources.Tramway;
            if (c.Nom == "Recharge")
                return Properties.Resources.Recharge;
            if (c.Nom == "Restauration")
                return Properties.Resources.Restauration;
            if (c.Nom == "Evacuation")
                return Properties.Resources.Evacuation;
            if (c.Nom == "Gaz")
                return Properties.Resources.Gaz;
            if (c.Nom == "Lanceflamme")
                return Properties.Resources.Lance_Flamme;
            if (c.Nom == "AmiEnfants")
                return Properties.Resources.Ami_des_Enfants;
            if (c.Nom == "Acide")
                return Properties.Resources.Attaque_acide;
            if (c.Nom == "Regeneration")
                return Properties.Resources.Regeneration;
            else
                throw new Exception();
        }

        private void ChangePictureBoxDesInThread(PictureBox pctB, Des d, bool visible)
        {
            MethodInvoker invoker = delegate
            {
                if (visible)
                    pctB.Image = WhichDesIHM(d);
                pctB.Visible = visible;
            };
            this.Invoke(invoker);
        }

        private void ChangeChckBDesInThread(CheckBox chB, bool enable)
        {
            MethodInvoker invoker = delegate
            {
                chB.Enabled = enable;
                chB.Visible = enable;
                if (!enable)
                {
                    chB.Checked = enable;
                }
            };
            this.Invoke(invoker);
        }


        public void UpdateIHM()
        {
            System.Threading.Thread.Sleep(250);
            while (!GameMaster.VerifVictoire().Item1)
            {
                if (PlateauDeJeu.JoueurDedans != null)
                {
                    try
                    {
                        UpdateIHMJoueurVille();
                        UpdateIHMJoueurActuel();
                        UpdateIHMJoueursForet();
                        UpdateIHMButton();
                        UpdateIHMDes();
                        UpdateIHMChckBDes();
                        UpdateChckBEventGame();
                        JeSuisToucheEtDedansChoix();
                        Gestionnaire_Cartes.UpdateGCartes();
                        UpdateIHMCartes();
                        UpdateIHMChckBCartes();
                        checkNBJoueurs();
                        UpdateIHMHistorique();
                    }
                    catch (ArgumentOutOfRangeException) { }
                    catch (IndexOutOfRangeException) { }


                }
            }
            System.Threading.Thread.Sleep(250);
            MessageBox.Show("Félicitations à " + GameMaster.VerifVictoire().Item2.Nom + " qui a gagné la partie !");
        }

        private void UpdateIHMHistorique()
        {
            ChangeRichTextBoxInThread(richTextBox1, Historique.ReadLine());
        }

        private void checkNBJoueurs()
        {
            if (nbJoueurs != PlateauDeJeu.JoueursDehors.Count + 1)
            {
                nbJoueurs = PlateauDeJeu.JoueursDehors.Count + 1;
                RefreshIHM();
            }
        }

        private void RefreshIHM()
        {
            AvatarJoueurForet1.Text = string.Empty;
            AvatarJoueurForet2.Text = string.Empty;
            AvatarJoueurForet3.Text = string.Empty;

            NomJoueurForet1.Text = string.Empty;
            NomJoueurForet2.Text = string.Empty;
            NomJoueurForet3.Text = string.Empty;

            VieJoueurForet1.Text = string.Empty;
            VieJoueurForet2.Text = string.Empty;
            VieJoueurForet3.Text = string.Empty;

            VicJoueurForet1.Text = string.Empty;
            VicJoueurForet2.Text = string.Empty;
            VicJoueurForet3.Text = string.Empty;

            EneJoueurForet1.Text = string.Empty;
            EneJoueurForet2.Text = string.Empty;
            EneJoueurForet3.Text = string.Empty;

            pictJoueurForet1.Image = null;
            pictJoueurForet2.Image = null;
            pictJoueurForet3.Image = null;

        }

        private void UpdateIHMJoueursForet()
        {
            ChangeLabelInThread(AvatarJoueurForet1, PlateauDeJeu.JoueursDehors[0].NomAvatar);
            ChangeLabelInThread(NomJoueurForet1, PlateauDeJeu.JoueursDehors[0].Nom);
            ChangeLabelInThread(VieJoueurForet1, PlateauDeJeu.JoueursDehors[0].Vie.ToString());
            ChangeLabelInThread(VicJoueurForet1, PlateauDeJeu.JoueursDehors[0].PtVictoire.ToString());
            ChangeLabelInThread(EneJoueurForet1, PlateauDeJeu.JoueursDehors[0].NbEnergie.ToString());
            ChangePictureBoxAvatarInThread(pictJoueurForet1, PlateauDeJeu.JoueursDehors[0]);

            if (nbJoueurs >= 3)
            {
                ChangeLabelInThread(AvatarJoueurForet2, PlateauDeJeu.JoueursDehors[1].NomAvatar);
                ChangeLabelInThread(NomJoueurForet2, PlateauDeJeu.JoueursDehors[1].Nom);
                ChangeLabelInThread(VieJoueurForet2, PlateauDeJeu.JoueursDehors[1].Vie.ToString());
                ChangeLabelInThread(VicJoueurForet2, PlateauDeJeu.JoueursDehors[1].PtVictoire.ToString());
                ChangeLabelInThread(EneJoueurForet2, PlateauDeJeu.JoueursDehors[1].NbEnergie.ToString());
                ChangePictureBoxAvatarInThread(pictJoueurForet2, PlateauDeJeu.JoueursDehors[1]);
            }

            if (nbJoueurs >= 4)
            {
                ChangeLabelInThread(AvatarJoueurForet3, PlateauDeJeu.JoueursDehors[2].NomAvatar);
                ChangeLabelInThread(NomJoueurForet3, PlateauDeJeu.JoueursDehors[2].Nom);
                ChangeLabelInThread(VieJoueurForet3, PlateauDeJeu.JoueursDehors[2].Vie.ToString());
                ChangeLabelInThread(VicJoueurForet3, PlateauDeJeu.JoueursDehors[2].PtVictoire.ToString());
                ChangeLabelInThread(EneJoueurForet3, PlateauDeJeu.JoueursDehors[2].NbEnergie.ToString());
                ChangePictureBoxAvatarInThread(pictJoueurForet3, PlateauDeJeu.JoueursDehors[2]);
            }
        }

        private void UpdateIHMJoueurVille()
        {
            ChangeLabelInThread(NomAvatarJoueurVille, PlateauDeJeu.JoueurDedans.NomAvatar);
            ChangeLabelInThread(NomJoueurVille, PlateauDeJeu.JoueurDedans.Nom);
            ChangeLabelInThread(lblNbPV, PlateauDeJeu.JoueurDedans.Vie.ToString());
            ChangeLabelInThread(lblNbVic, PlateauDeJeu.JoueurDedans.PtVictoire.ToString());
            ChangeLabelInThread(lblNbEne, PlateauDeJeu.JoueurDedans.NbEnergie.ToString());
            ChangePictureBoxAvatarInThread(pictBoxJoueurVile, PlateauDeJeu.JoueurDedans);
        }

        private void UpdateIHMJoueurActuel()
        {
            ChangeLabelInThread(NomAvatarJoueurActuel, GameMaster.joueurActuel.NomAvatar);
            ChangeLabelInThread(NomJoueurActuel, GameMaster.joueurActuel.Nom);
            ChangeLabelInThread(lblNbPVJActuel, GameMaster.joueurActuel.Vie.ToString());
            ChangeLabelInThread(lblNbVicJActuel, GameMaster.joueurActuel.PtVictoire.ToString());
            ChangeLabelInThread(lblNbEneJActuel, GameMaster.joueurActuel.NbEnergie.ToString());
            ChangePictureBoxAvatarInThread(pictureBox7, GameMaster.joueurActuel);
        }

        private void UpdateIHMButton()
        {
            if (GameMaster.joueurActuel.Nom == joueurLocal.Nom)
            {
                if (EventGame.phaseDesTerminee)
                {
                    ChangeButtonInThread(btnLancer, false);

                    if (EventGame.blockFinDeTour)
                    {
                        ChangeButtonInThread(btnFinTour, false);
                        ChangeButtonInThread(BtnAcheter, false);
                        ChangeButtonInThread(btnVendre, false);
                    }
                    else
                    {
                        ChangeButtonInThread(btnFinTour, true);
                        ChangeButtonInThread(BtnAcheter, true);
                        ChangeButtonInThread(btnVendre, true);
                    }
                }
                else
                {
                    ChangeButtonInThread(BtnAcheter, false);
                    ChangeButtonInThread(btnFinTour, false);
                    ChangeButtonInThread(btnVendre, false);
                    ChangeButtonInThread(btnLancer, true);
                }
            }
            else
            {
                ChangeButtonInThread(BtnAcheter, false);
                ChangeButtonInThread(btnFinTour, false);
                ChangeButtonInThread(btnLancer, false);
                ChangeButtonInThread(btnVendre, false);
            }
        }

        private void UpdateIHMDes()
        {
            if (EventGame.affichageLance && Gestionnaire_Des.ListDes.Count > 0)
            {
                try
                {
                    ChangePictureBoxDesInThread(pictDe1, Gestionnaire_Des.ListDes[0], true);
                    ChangePictureBoxDesInThread(pictDe2, Gestionnaire_Des.ListDes[1], true);
                    ChangePictureBoxDesInThread(pictDe3, Gestionnaire_Des.ListDes[2], true);
                    ChangePictureBoxDesInThread(pictDe4, Gestionnaire_Des.ListDes[3], true);
                    ChangePictureBoxDesInThread(pictDe5, Gestionnaire_Des.ListDes[4], true);
                    ChangePictureBoxDesInThread(pictDe6, Gestionnaire_Des.ListDes[5], true);
                }
                catch (Exception ex)
                { }
            }
            else
            {
                ChangePictureBoxDesInThread(pictDe1, null, false);
                ChangePictureBoxDesInThread(pictDe2, null, false);
                ChangePictureBoxDesInThread(pictDe3, null, false);
                ChangePictureBoxDesInThread(pictDe4, null, false);
                ChangePictureBoxDesInThread(pictDe5, null, false);
                ChangePictureBoxDesInThread(pictDe6, null, false);
            }
        }

        private void UpdateChckBEventGame()
        {
            if (checkBox1.Checked)
                EventGame.rerollDes1 = true;
            if (!checkBox1.Checked)
                EventGame.rerollDes1 = false;

            if (checkBox2.Checked)
                EventGame.rerollDes2 = true;
            if (!checkBox2.Checked)
                EventGame.rerollDes2 = false;

            if (checkBox3.Checked)
                EventGame.rerollDes3 = true;
            if (!checkBox3.Checked)
                EventGame.rerollDes3 = false;

            if (checkBox4.Checked)
                EventGame.rerollDes4 = true;
            if (!checkBox4.Checked)
                EventGame.rerollDes4 = false;

            if (checkBox5.Checked)
                EventGame.rerollDes5 = true;
            if (!checkBox5.Checked)
                EventGame.rerollDes5 = false;

            if (checkBox6.Checked)
                EventGame.rerollDes6 = true;
            if (!checkBox6.Checked)
                EventGame.rerollDes6 = false;


        }

        private void UpdateIHMChckBDes()
        {
            if (GameMaster.joueurActuel.Nom == joueurLocal.Nom && EventGame.premierLance && !EventGame.phaseDesTerminee)
            {
                ChangeChckBDesInThread(checkBox1, true);
                ChangeChckBDesInThread(checkBox2, true);
                ChangeChckBDesInThread(checkBox3, true);
                ChangeChckBDesInThread(checkBox4, true);
                ChangeChckBDesInThread(checkBox5, true);
                ChangeChckBDesInThread(checkBox6, true);
            }
            else
            {
                ChangeChckBDesInThread(checkBox1, false);
                ChangeChckBDesInThread(checkBox2, false);
                ChangeChckBDesInThread(checkBox3, false);
                ChangeChckBDesInThread(checkBox4, false);
                ChangeChckBDesInThread(checkBox5, false);
                ChangeChckBDesInThread(checkBox6, false);
            }
        }

        private void UpdateIHMChckBCartes()
        {
            if (GameMaster.joueurActuel.Nom == joueurLocal.Nom && EventGame.phaseDesTerminee)
            {
                if (Gestionnaire_Cartes.ListeCarteVisibles.Count > 0)
                    ChangeChckBDesInThread(checkBox7, true);
                else
                    ChangeChckBDesInThread(checkBox7, false);

                if (Gestionnaire_Cartes.ListeCarteVisibles.Count > 1)
                    ChangeChckBDesInThread(checkBox8, true);
                else
                    ChangeChckBDesInThread(checkBox8, false);

                if (Gestionnaire_Cartes.ListeCarteVisibles.Count > 2)
                    ChangeChckBDesInThread(checkBox9, true);
                else
                    ChangeChckBDesInThread(checkBox9, false);


                if (GameMaster.joueurActuel.ListCartes.Count > 0)
                    ChangeChckBDesInThread(checkBox10, true);
                else
                    ChangeChckBDesInThread(checkBox10, false);

                if (GameMaster.joueurActuel.ListCartes.Count > 1)
                    ChangeChckBDesInThread(checkBox11, true);
                else
                    ChangeChckBDesInThread(checkBox11, false);

                if (GameMaster.joueurActuel.ListCartes.Count > 2)
                    ChangeChckBDesInThread(checkBox12, true);
                else
                    ChangeChckBDesInThread(checkBox12, false);
            }
            else
            {
                ChangeChckBDesInThread(checkBox7, false);
                ChangeChckBDesInThread(checkBox8, false);
                ChangeChckBDesInThread(checkBox9, false);

                ChangeChckBDesInThread(checkBox10, false);
                ChangeChckBDesInThread(checkBox11, false);
                ChangeChckBDesInThread(checkBox12, false);
            }
        }

        private void JeSuisToucheEtDedansChoix()
        {
            if (EventGame.argJeSuisTouche)
            {
                DialogResult dialogResult = MessageBox.Show(joueurLocal.Nom + ", vous avez été baffé alors que vous êtes dans Tokyo," +
                    " voulez-vous échanger votre place avec " + GameMaster.joueurActuel.Nom, "Baffe !", MessageBoxButtons.YesNo);
                if (dialogResult == DialogResult.Yes)
                {
                    EventGame.jeVeuxSortir = true;
                }
                else if (dialogResult == DialogResult.No)
                {
                    EventGame.jeVeuxSortir = false;
                }
                EventGame.argJeSuisTouche = false;
                EventGame.reponseSortir = true;
            }
        }

        private void UpdateIHMCartes()
        {
            UpdateIHMCartesVisibles();
            UpdateIHMCartesCachees();
            UpdateIHMCartesJActuel();
            UpdateIHMCartesJDedans();
        }

        private void UpdateIHMCartesJDedans()
        {
            if (PlateauDeJeu.JoueurDedans.ListCartes.Count > 0)
            {
                try
                {
                    if (PlateauDeJeu.JoueurDedans.ListCartes.Count == 1)
                    {
                        ChangePictureBoxCarteInThread(CarteJoueurVille1, PlateauDeJeu.JoueurDedans.ListCartes[0]);
                        ChangePictureBoxCarteInThread(CarteJoueurVille2, null);
                        ChangePictureBoxCarteInThread(CarteJoueurVille3, null);
                    }
                    if (PlateauDeJeu.JoueurDedans.ListCartes.Count == 2)
                    {
                        ChangePictureBoxCarteInThread(CarteJoueurVille1, PlateauDeJeu.JoueurDedans.ListCartes[0]);
                        ChangePictureBoxCarteInThread(CarteJoueurVille2, PlateauDeJeu.JoueurDedans.ListCartes[1]);
                        ChangePictureBoxCarteInThread(CarteJoueurVille3, null);
                    }

                    if (PlateauDeJeu.JoueurDedans.ListCartes.Count == 3)
                    {
                        ChangePictureBoxCarteInThread(CarteJoueurVille1, PlateauDeJeu.JoueurDedans.ListCartes[0]);
                        ChangePictureBoxCarteInThread(CarteJoueurVille2, PlateauDeJeu.JoueurDedans.ListCartes[1]);
                        ChangePictureBoxCarteInThread(CarteJoueurVille3, PlateauDeJeu.JoueurDedans.ListCartes[2]);
                    }
                }
                catch (IndexOutOfRangeException ex) { }
                catch (ArgumentOutOfRangeException ex) { }
            }
            else
            {
                ChangePictureBoxCarteInThread(CarteJoueurVille1, null);
                ChangePictureBoxCarteInThread(CarteJoueurVille2, null);
                ChangePictureBoxCarteInThread(CarteJoueurVille3, null);
            }
        }

        private void UpdateIHMCartesJActuel()
        {
            if (GameMaster.joueurActuel.ListCartes.Count > 0)
            {

                try
                {
                    if (GameMaster.joueurActuel.ListCartes.Count == 1)
                    {
                        ChangePictureBoxCarteInThread(CarteJoueurActuel1, GameMaster.joueurActuel.ListCartes[0]);
                        ChangePictureBoxCarteInThread(CarteJoueurActuel2, null);
                        ChangePictureBoxCarteInThread(CarteJoueurActuel3, null);
                    }
                    if (GameMaster.joueurActuel.ListCartes.Count == 2)
                    {
                        ChangePictureBoxCarteInThread(CarteJoueurActuel1, GameMaster.joueurActuel.ListCartes[0]);
                        ChangePictureBoxCarteInThread(CarteJoueurActuel2, GameMaster.joueurActuel.ListCartes[1]);
                        ChangePictureBoxCarteInThread(CarteJoueurActuel3, null);
                    }

                    if (GameMaster.joueurActuel.ListCartes.Count == 3)
                    {
                        ChangePictureBoxCarteInThread(CarteJoueurActuel1, GameMaster.joueurActuel.ListCartes[0]);
                        ChangePictureBoxCarteInThread(CarteJoueurActuel2, GameMaster.joueurActuel.ListCartes[1]);
                        ChangePictureBoxCarteInThread(CarteJoueurActuel3, GameMaster.joueurActuel.ListCartes[2]);
                    }
                }
                catch (IndexOutOfRangeException ex) { }
                catch (ArgumentOutOfRangeException ex) { }
            }
            else
            {
                ChangePictureBoxCarteInThread(CarteJoueurActuel1, null);
                ChangePictureBoxCarteInThread(CarteJoueurActuel2, null);
                ChangePictureBoxCarteInThread(CarteJoueurActuel3, null);
            }
        }

        private void UpdateIHMCartesCachees()
        {
            if (Gestionnaire_Cartes.ListeCarteCachees.Count == 0)
            {
                MethodInvoker invoker = delegate
                {
                    pictRefillShop.Image = null;
                };
                this.Invoke(invoker);
            }
        }

        private void UpdateIHMCartesVisibles()
        {

            if (Gestionnaire_Cartes.ListeCarteVisibles.Count > 0)
            {
                try
                {
                    if (Gestionnaire_Cartes.ListeCarteVisibles.Count == 1)
                    {
                        ChangePictureBoxCarteInThread(pictShop1, Gestionnaire_Cartes.ListeCarteVisibles[0]);
                        ChangePictureBoxCarteInThread(pictShop2, null);
                        ChangePictureBoxCarteInThread(pictShop3, null);
                    }
                    if (Gestionnaire_Cartes.ListeCarteVisibles.Count == 2)
                    {
                        ChangePictureBoxCarteInThread(pictShop1, Gestionnaire_Cartes.ListeCarteVisibles[0]);
                        ChangePictureBoxCarteInThread(pictShop2, Gestionnaire_Cartes.ListeCarteVisibles[1]);
                        ChangePictureBoxCarteInThread(pictShop3, null);
                    }

                    if (Gestionnaire_Cartes.ListeCarteVisibles.Count == 3)
                    {
                        ChangePictureBoxCarteInThread(pictShop1, Gestionnaire_Cartes.ListeCarteVisibles[0]);
                        ChangePictureBoxCarteInThread(pictShop2, Gestionnaire_Cartes.ListeCarteVisibles[1]);
                        ChangePictureBoxCarteInThread(pictShop3, Gestionnaire_Cartes.ListeCarteVisibles[2]);
                    }
                }
                catch (IndexOutOfRangeException ex) { }
                catch (ArgumentOutOfRangeException ex) { }

            }
            else
            {
                ChangePictureBoxCarteInThread(pictShop1, null);
                ChangePictureBoxCarteInThread(pictShop2, null);
                ChangePictureBoxCarteInThread(pictShop3, null);
            }
        }

        private void OpenInfoCarte(List<Carte> listCartes)
        {
            infosCarte = new InfosCartes(listCartes);
            infosCarte.ShowDialog();
        }

        private void btnLancer_Click(object sender, EventArgs e)
        {
            if (EventGame.premierLance)
                EventGame.phaseReroll = true;
            EventGame.premierLance = true;
        }


        #region MouseHover

        private void pictShop1_MouseEnter(object sender, EventArgs e)
        {
            OpenInfoCarte(Gestionnaire_Cartes.ListeCarteVisibles);
        }

        private void pictShop2_MouseHover(object sender, EventArgs e)
        {
            OpenInfoCarte(Gestionnaire_Cartes.ListeCarteVisibles);

        }

        private void pictShop3_MouseHover(object sender, EventArgs e)
        {
            OpenInfoCarte(Gestionnaire_Cartes.ListeCarteVisibles);

        }

        private void pictJoueurForet1_MouseHover(object sender, EventArgs e)
        {
            try
            {
                Joueur joueur = PlateauDeJeu.JoueursDehors.Find(j => j.Nom == NomJoueurForet1.Text);
                OpenInfoCarte(joueur.ListCartes);
            }
            catch { }
        }


        private void pictJoueurForet2_MouseHover(object sender, EventArgs e)
        {
            try
            {
                Joueur joueur = PlateauDeJeu.JoueursDehors.Find(j => j.Nom == NomJoueurForet2.Text);
                OpenInfoCarte(joueur.ListCartes);
            }
            catch { }
        }


        private void pictJoueurForet3_MouseHover(object sender, EventArgs e)
        {
            try
            {
                Joueur joueur = PlateauDeJeu.JoueursDehors.Find(j => j.Nom == NomJoueurForet3.Text);
                OpenInfoCarte(joueur.ListCartes);
            }
            catch { }
        }


        private void CarteJoueurVille1_MouseHover(object sender, EventArgs e)
        {
            Joueur joueur = PlateauDeJeu.JoueurDedans;
            OpenInfoCarte(joueur.ListCartes);
        }

        private void CarteJoueurVille2_MouseHover(object sender, EventArgs e)
        {
            Joueur joueur = PlateauDeJeu.JoueurDedans;
            OpenInfoCarte(joueur.ListCartes);
        }

        private void CarteJoueurVille3_MouseHover(object sender, EventArgs e)
        {
            Joueur joueur = PlateauDeJeu.JoueurDedans;
            OpenInfoCarte(joueur.ListCartes);
        }

        private void CarteJoueurActuel1_MouseHover(object sender, EventArgs e)
        {
            Joueur joueur = GameMaster.joueurActuel;
            OpenInfoCarte(joueur.ListCartes);
        }

        private void CarteJoueurActuel2_MouseHover(object sender, EventArgs e)
        {
            Joueur joueur = GameMaster.joueurActuel;
            OpenInfoCarte(joueur.ListCartes);
        }

        private void CarteJoueurActuel3_MouseHover(object sender, EventArgs e)
        {
            Joueur joueur = GameMaster.joueurActuel;
            OpenInfoCarte(joueur.ListCartes);
        }
        #endregion

        #region Events Poubelles
        private void pictJoueurForet1_MouseLeave(object sender, EventArgs e)
        {

        }


        private void pictJoueurForet3_Click(object sender, EventArgs e)
        {

        }

        private void radioButton2_CheckedChanged(object sender, EventArgs e)
        {

        }

        private void pictShop1_MouseLeave(object sender, EventArgs e)
        {

        }

        private void pictShop1_MouseHover(object sender, EventArgs e)
        {

        }


        private void gbJForet1_MouseHover(object sender, EventArgs e)
        {
        }


        private void CarteJoueurActuel1_Click(object sender, EventArgs e)
        {
        }


        private void CarteJoueurActuel2_Click(object sender, EventArgs e)
        {
        }

        private void CarteJoueurActuel3_Click(object sender, EventArgs e)
        {
        }
        #endregion

        private void BtnAcheter_Click(object sender, EventArgs e)
        {
            int totalEnergie = 0;
            if (checkBox7.Checked)
            {
                EventGame.achatCarte1 = true;
                totalEnergie += Gestionnaire_Cartes.ListeCarteVisibles[0].Prix;
            }
            else
            {
                EventGame.achatCarte1 = false;
            }

            if (checkBox8.Checked)
            {
                EventGame.achatCarte2 = true;
                totalEnergie += Gestionnaire_Cartes.ListeCarteVisibles[1].Prix;
            }
            else
            {
                EventGame.achatCarte2 = false;
            }

            if (checkBox9.Checked)
            {
                EventGame.achatCarte3 = true;
                totalEnergie += Gestionnaire_Cartes.ListeCarteVisibles[2].Prix;
            }
            else
            {
                EventGame.achatCarte3 = false;
            }

            if (totalEnergie > GameMaster.joueurActuel.NbEnergie)
            {
                MessageBox.Show("Vous n'avez pas assez d'énergie !");
            }
            else
            {
                EventGame.achatCarte = true;
                EventGame.phaseCarte = true;
            }

        }

        private void btnVendre_Click(object sender, EventArgs e)
        {
            if (checkBox10.Checked)
                EventGame.venteCarte1 = true;
            if (checkBox11.Checked)
                EventGame.venteCarte2 = true;
            if (checkBox12.Checked)
                EventGame.venteCarte3 = true;


            EventGame.venteCarte = true;
            EventGame.phaseCarte = true;
        }

        private void btnFinTour_Click(object sender, EventArgs e)
        {
            EventGame.finDeTour = true;
            EventGame.phaseCarte = true;
            //   EventGame.achatCarte = true;
            //   EventGame.venteCarte = true;
        }

    }
}

﻿namespace QOT_UserInterface
{
    partial class RejoindreLobby
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.txtIPHote = new System.Windows.Forms.TextBox();
            this.btnRejointPartie = new System.Windows.Forms.Button();
            this.btnRetour = new System.Windows.Forms.Button();
            this.lblPseudo = new System.Windows.Forms.Label();
            this.pictBLogo = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.pictBLogo)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(573, 336);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(94, 13);
            this.label1.TabIndex = 2;
            this.label1.Text = "IP du joueur hôte: ";
            // 
            // txtIPHote
            // 
            this.txtIPHote.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.txtIPHote.Location = new System.Drawing.Point(673, 333);
            this.txtIPHote.Name = "txtIPHote";
            this.txtIPHote.Size = new System.Drawing.Size(169, 20);
            this.txtIPHote.TabIndex = 3;
            // 
            // btnRejointPartie
            // 
            this.btnRejointPartie.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.btnRejointPartie.Location = new System.Drawing.Point(660, 397);
            this.btnRejointPartie.Name = "btnRejointPartie";
            this.btnRejointPartie.Size = new System.Drawing.Size(145, 49);
            this.btnRejointPartie.TabIndex = 4;
            this.btnRejointPartie.Text = "Rejoindre la partie";
            this.btnRejointPartie.UseVisualStyleBackColor = true;
            this.btnRejointPartie.Click += new System.EventHandler(this.btnRejointPartie_Click);
            // 
            // btnRetour
            // 
            this.btnRetour.Location = new System.Drawing.Point(1103, 573);
            this.btnRetour.Name = "btnRetour";
            this.btnRetour.Size = new System.Drawing.Size(75, 23);
            this.btnRetour.TabIndex = 5;
            this.btnRetour.Text = "Retour";
            this.btnRetour.UseVisualStyleBackColor = true;
            this.btnRetour.Click += new System.EventHandler(this.btnRetour_Click);
            // 
            // lblPseudo
            // 
            this.lblPseudo.AutoSize = true;
            this.lblPseudo.Location = new System.Drawing.Point(711, 281);
            this.lblPseudo.Name = "lblPseudo";
            this.lblPseudo.Size = new System.Drawing.Size(35, 13);
            this.lblPseudo.TabIndex = 6;
            this.lblPseudo.Text = "label2";
            // 
            // pictBLogo
            // 
            this.pictBLogo.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.pictBLogo.Image = global::QOT_UserInterface.Properties.Resources.logo;
            this.pictBLogo.Location = new System.Drawing.Point(420, 40);
            this.pictBLogo.Name = "pictBLogo";
            this.pictBLogo.Size = new System.Drawing.Size(656, 182);
            this.pictBLogo.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.pictBLogo.TabIndex = 1;
            this.pictBLogo.TabStop = false;
            // 
            // RejoindreLobby
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(1387, 640);
            this.Controls.Add(this.lblPseudo);
            this.Controls.Add(this.btnRetour);
            this.Controls.Add(this.btnRejointPartie);
            this.Controls.Add(this.txtIPHote);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.pictBLogo);
            this.Name = "RejoindreLobby";
            this.Text = "RejoindreLobby";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.RejoindreLobby_FormClosing);
            this.Load += new System.EventHandler(this.RejoindreLobby_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pictBLogo)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox pictBLogo;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtIPHote;
        private System.Windows.Forms.Button btnRejointPartie;
        private System.Windows.Forms.Button btnRetour;
        private System.Windows.Forms.Label lblPseudo;
    }
}
﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using QOT_Application;
using QueenOfTokyo_Infrastructure;

namespace QOT_UserInterface
{
    public partial class RejoindreLobby : Form
    {
        private Accueil _Acc;
        private string pseudo1;
        public RejoindreLobby(Accueil Acc)
        {
            InitializeComponent();

            this._Acc = Acc;
            this.pseudo1 = this._Acc.txtPseudo.Text;
            this.lblPseudo.Text = this.pseudo1;
        }

        private void RejoindreLobby_Load(object sender, EventArgs e)
        {
        }

        private void btnRetour_Click(object sender, EventArgs e)
        {
            this._Acc.Visible = true;
            this.Close();
        }

        private void RejoindreLobby_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (this.ActiveControl.Text != "Retour")
            {
                Application.Exit();
            }
        }

        private void btnRejointPartie_Click(object sender, EventArgs e)
        {
            try
            {
                lblPseudo.Text = PhaseLobby.Connexion(txtIPHote.Text,lblPseudo.Text);

                ChoixPerso choixPerso = new ChoixPerso(txtIPHote.Text,pseudo1);
                this.Visible = false;
                choixPerso.ShowDialog();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);      
            }
               

        }
    }
}

﻿using QOT_Domain_Joueur;
using QueenOfTokyo_Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QOT_Application
{
    public static class PhaseDebutPartie
    {
        public static List<Joueur> ResultatsLanceDes()
        {
            List<Joueur> res = new List<Joueur>();
            string reception = Client.ReceiveFromServer();

            List<string> listj = reception.Split('!').ToList();

            foreach (string str in listj)
            {
                Joueur j = new Joueur();
                j = j.StringToJoueur(str);
                res.Add(j);
            } 
            return res;
        }

        public static void ServerDebutPartie()
        {
            if(HostServer.amIHost)
                HostServer.DebutPartie();
        }

    }
}

﻿using QOT_Domain_Carte;
using QueenOfTokyo_Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;

namespace QOT_Application
{
    public static class PhaseJeu
    {
        public static void InitCartes()
        {
            if (HostServer.amIHost)
            {
                Task.Factory.StartNew(() => HostServer.GenerateCartes());
            }
            string message = Client.ReceiveFromServer();
            Gestionnaire_Cartes.StringToGCartes(message);
        }


        public static void LaunchGameplay()
        {
            if (HostServer.amIHost)
            {
                foreach (Socket s in HostServer.dicoSocketJoueur.Keys)
                {
                    Task.Factory.StartNew(() => HostServer.InGame(s));
                }

            }
            
        }

    }
}

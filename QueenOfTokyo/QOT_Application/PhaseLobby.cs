﻿using QueenOfTokyo_Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QOT_Application
{
    public static class PhaseLobby
    {
        public static string Connexion(string ip, string pseudo)
        {
            Client.InitClient(ip, 0);
            Client.SendToServer(pseudo);
            return Client.ReceiveFromServer();
        }

        public static int DemandeNbJoueur()
        {
            Client.SendToServer("NOMBREJOUEURS");          
            int nbJoueur = int.Parse(Client.ReceiveFromServer());
            return nbJoueur;
        }
        public static string MonNom()
        {
            Client.SendToServer("MONNOM");
            string res = Client.ReceiveFromServer();
            return res;
        }
    
        public static Dictionary<string,string> ReceptionNomsAvatars()
        {
            Client.SendToServer("LISTNOMSAVATARS");
            string allNamesAvatars = Client.ReceiveFromServer();
            Dictionary<string, string> res = new Dictionary<string, string>();
            StringBuilder nom = new StringBuilder();
            StringBuilder avatar = new StringBuilder();
            bool avatarPart = false;

            for (int i = 0; i < allNamesAvatars.Length; i++)
            {
                if (allNamesAvatars[i] == ';')
                {
                    res.Add(nom.ToString(), avatar.ToString());
                    nom.Clear();
                    avatar.Clear();
                    avatarPart = false;
                    continue;
                }
                else if (allNamesAvatars[i] == ':')
                {
                    avatarPart = true;
                    continue;
                }
                else if (!avatarPart)
                {
                    nom.Append(allNamesAvatars[i]);
                }
                else
                {
                    avatar.Append(allNamesAvatars[i]);
                }

            }
            return res;
        }

        public static bool AvatarChoisi(string nomAvatar)
        {
            Client.SendToServer("AVATAR;"+nomAvatar);
            string reponse = Client.ReceiveFromServer();
            if (reponse == "OK")
            {
                return true;
            }
            else
            {
                return false;
            }
            
        }

        public static void FinPhase()
        {
            Client.SendToServer("FIN");
        }
    }
}

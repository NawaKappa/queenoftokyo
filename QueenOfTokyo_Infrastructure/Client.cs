﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;

namespace QueenOfTokyo_Infrastructure
{
    public static class Client
    {
        private static TcpClient tcpCnt = new TcpClient();
        private static Stream stream;

        /// <summary>
        /// Initialise le client pour la communication avec le server.
        /// </summary>
        /// <param name="adresseIPServ">Adresse IP du server</param>
        /// <param name="port">Port du server</param>
        public static void InitClient(string adresseIPServ, int port)
        {
            try
            {
                tcpCnt.Connect(adresseIPServ, 8001);
                stream = tcpCnt.GetStream();
            }

            catch (Exception e)
            {
                throw e;
            }
        }

        public static void CleanClient()
        {
            stream.Close();
        }


        /// <summary>
        /// Envoie un message au server.
        /// </summary>
        /// <param name="message"></param>
        public static void SendToServer(string message)
        {
            byte[] bMessage = Service_Infrastructure.StringToByte(message);           
            stream.Write(bMessage, 0, bMessage.Length);
        }


        /// <summary>
        /// Receptionne le message du server.
        /// </summary>
        /// <returns></returns>
        public static string ReceiveFromServer()
        {
            byte[] recepteurMessage = new byte[300];
            stream.Read(recepteurMessage, 0, 300);
            return Service_Infrastructure.ByteToString(recepteurMessage);               
        }
    }
}

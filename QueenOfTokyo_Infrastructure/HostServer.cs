﻿using QOT_Domain_Carte;
using QOT_Domain_Joueur;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;

namespace QueenOfTokyo_Infrastructure
{
    public static class HostServer
    {
        static void Main(string[] args)
        {
            HostMain(1);
            Console.ReadLine();
        }

        public static Dictionary<Socket, Joueur> dicoSocketJoueur = new Dictionary<Socket, Joueur>();
        private static IPAddress iPServ = null;
        private static TcpListener listener = null;
        public static int nbJoueurPresent = 0;
        private static int nbJoueurMax = 0;
        public static bool amIHost = false;

        /// <summary>
        /// Fontion principal du server hôte
        /// </summary>
        /// <param name="nbJoueur">Nombre de joueurs au début de la partie.</param>
        public static void HostMain(object nbJoueurs)
        {
            ServerInit(nbJoueurs);
           // TCPIPCleaner(listSocket, listener);
            Console.WriteLine("Server Closed.");
        }


        /// <summary>
        /// Initialise le server
        /// </summary>
        public static void ServerInit(object nbJoueur)
        {
            amIHost = true;
            nbJoueurMax = (int)nbJoueur;
            if (nbJoueurMax < 1)
            {
                throw new ArgumentException("Le nombre de joueur doit être suppérieur à 2");
            }

            try
            {
                iPServ = IPAddress.Parse(Service_Infrastructure.GetLocalIPAddress());

                Console.WriteLine(iPServ.ToString());

                listener = new TcpListener(iPServ, 8001);
                listener.Start();
                string allNames = string.Empty;
                while (nbJoueurPresent < nbJoueurMax)
                {
                    Socket socketJoueur = listener.AcceptSocket();
                    string nom = ReceptionMessage(socketJoueur);

                    var result = dicoSocketJoueur.Values.ToList().FirstOrDefault(j => j.Nom == nom);
                    while (result != null)
                    {
                        nom += "Bis";
                        result = dicoSocketJoueur.Values.ToList().FirstOrDefault(j => j.Nom == nom);
                    }

                    SendTo(nom, socketJoueur);

                    Joueur joueur = new Joueur(nom);
                    dicoSocketJoueur.Add(socketJoueur,joueur);
                    nbJoueurPresent++;


                    Task.Factory.StartNew(() => RequetesLobby(socketJoueur)); 
                   
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                TCPIPCleaner();
            }
        }


        private static void RequetesLobby(Socket socketJoueur)
        {
            bool finished = false;
            while (!finished)
            {
                string messageRecu = ReceptionMessage(socketJoueur);

                if (messageRecu == "NOMBREJOUEURS")
                {
                    SendTo(nbJoueurMax.ToString(), socketJoueur);
                }

                else if (messageRecu == "LISTNOMSAVATARS")
                {
                    StringBuilder listnom = new StringBuilder();

                    lock (dicoSocketJoueur)
                    {
                        foreach (Joueur joueur in dicoSocketJoueur.Values)
                        {
                            listnom.Append(joueur.Nom + ":" + joueur.NomAvatar +";");
                        }
                    }

                    SendTo(listnom.ToString(), socketJoueur);
                }

                else if (messageRecu == "MONNOM")
                {
                    SendTo(dicoSocketJoueur[socketJoueur].Nom, socketJoueur);
                }

                else if (messageRecu.Contains("AVATAR;"))
                {
                    int index = messageRecu.IndexOf(';')+1;
                    string avatar = messageRecu.Substring(index);

                    lock (dicoSocketJoueur)
                    {
                        var result = dicoSocketJoueur.Values.ToList().FirstOrDefault(j => j.NomAvatar == avatar);
                        if (result == null)
                        {
                            dicoSocketJoueur[socketJoueur].NomAvatar = avatar;
                            SendTo("OK", socketJoueur);
                        }
                        else
                        {
                            SendTo("NOTOK", socketJoueur);
                        }
                    }
                }

                if (messageRecu == "END")
                {
                    finished = true;
                }
            }
        }


        public static void DebutPartie()
        {
            List<int> listDesDejaAttribue = new List<int>();
            StringBuilder MessageListJoueur = new StringBuilder();
            Random rdn = new Random();
            foreach (var pair in dicoSocketJoueur)
            {
                int valeur = rdn.Next(1, 7);
                while (listDesDejaAttribue.Contains(valeur))
                {
                    valeur = rdn.Next(1, 7);
                }
                listDesDejaAttribue.Add(valeur);
                pair.Value.DesDebut = valeur;
            }
            foreach (Joueur j in dicoSocketJoueur.Values)
            {
                MessageListJoueur.Append(j.ToString() + "!");
            }
            MessageListJoueur.Remove(MessageListJoueur.Length - 1,1);
            SendToAll(MessageListJoueur.ToString());            
        }



        public static void GenerateCartes()
        {
            string listCarte = Gestionnaire_Cartes.GenerateCartes();
            
            SendToAll(listCarte);
        }

        public static void InGame(Socket socketJoueur)
        {
            bool finished = false;
            while (!finished)
            {

                string messageRecu = ReceptionMessage(socketJoueur);
               // Console.WriteLine("Message reçu de : " +dicoSocketJoueur[socketJoueur].Nom + " : " +messageRecu);
                if (messageRecu.Contains("LISTDES"))
                {
                    SendToAll(messageRecu.Split(':').ToList()[1]);
                }
                if (messageRecu.Contains("REROLL"))
                {
                    SendToAll(messageRecu.Split(':').ToList()[1]);
                }
                if (messageRecu.Contains("JESORS"))
                {
                    SendToAll("swap");
                }
                if (messageRecu.Contains("JERESTE"))
                {
                    SendToAll("noswap");
                }
                if (messageRecu.Contains("FINTOUR"))
                {
                    SendToAll(messageRecu);
                }
                if (messageRecu.Contains("PHASEACHAT"))
                {
                    SendToAll(messageRecu);
                }
                if (messageRecu.Contains("ACHAT:"))
                {
                    SendToAll(messageRecu.Split(':').ToList()[1]);
                }

                if (messageRecu.Contains("PHASEVENTE"))
                {
                    SendToAll(messageRecu);
                }
                if (messageRecu.Contains("VENTE:"))
                {
                    SendToAll(messageRecu.Split(':').ToList()[1]);
                }
            }
        }


        /// <summary>
        /// Envoie un message à tous les sockets
        /// </summary>
        /// <param name="message">Message à envoyer</param>
        public static void SendToAll(string message)
        {
            foreach (Socket socketJoueur in dicoSocketJoueur.Keys.ToList())
            {
                SendTo(message, socketJoueur);
            }
        }


        /// <summary>
        /// Envoie un message à un utilisateur
        /// </summary>
        /// <param name="message">Message à envoyer</param>
        /// <param name="socket">Socket qui va receptionner le message</param>
        /// <returns></returns>
        public static bool SendTo(string message, Socket socket)
        {
            try
            {
                socket.Send(Service_Infrastructure.StringToByte(message));
                return true;
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                return false;
            }
           
        }

        /// <summary>
        /// Reception d'un message d'un socket
        /// </summary>
        /// <param name="socket">Socket à écouter</param>
        /// <returns>Message en octets</returns>
        public static string ReceptionMessage(Socket socket)
        {
            byte[] recepteurMessage = new byte[300];

            socket.Receive(recepteurMessage);

            string res = Service_Infrastructure.ByteToString(recepteurMessage);

            return res;           
        }

        /// <summary>
        /// Ferme les sockets et le listener
        /// </summary>
        /// <param name="listSocket">Liste des sockets à fermer</param>
        /// <param name="listener">Listener à fermer</param>
        public static void TCPIPCleaner()
        {
            foreach (Socket sock in dicoSocketJoueur.Keys)
            {
                sock.Close();
            }
            try
            {
                listener.Stop();
            }
            catch (ArgumentNullException)
            {
            }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;

namespace QueenOfTokyo_Infrastructure
{
    class Program
    {
        static void Main(string[] args)
        {
            server();
            Console.ReadLine();
        }
        public static void server()
        {
            try
            {
                
                string externalip = new WebClient().DownloadString("http://icanhazip.com").Replace("\n", "");
                IPAddress iPServ = IPAddress.Parse(externalip);
                Console.WriteLine(externalip);

                /* Initializes the Listener */
                TcpListener myList = new TcpListener(iPServ, 8001);

                myList.Start();

                Socket s1 = myList.AcceptSocket();

                byte[] b = new byte[100];
                int k = s1.Receive(b);

                for (int i = 0; i < k; i++)
                {
                    Console.Write(Convert.ToChar(b[i]).ToString());
                }

                ASCIIEncoding asen = new ASCIIEncoding();
                s1.Send(asen.GetBytes("The string was recieved by the server."));
                Console.WriteLine("\nSent Acknowledgement");
                /*blblbl*/
                /* clean up */
                s1.Close();
                myList.Stop();
            }
            finally
            { }
        }

        private static string GetLocalIPAddress()
        {
            {
                IPHostEntry host;
                string localIP = "?";
                host = Dns.GetHostEntry(Dns.GetHostName());
                foreach (IPAddress ip in host.AddressList)
                {
                    if (ip.AddressFamily == AddressFamily.InterNetwork)
                    {
                        localIP = ip.ToString();
                    }
                }
                return localIP;
            }
        }
        public static void client()
        {
            try
            {
                TcpClient tcpclnt = new TcpClient();
                // MessageBox.Show("Connecting.....");

                tcpclnt.Connect("192.168.42.152", 8001);
                // use the ipaddress as in the server program

                Console.WriteLine("Connected");
                Console.Write("Enter the string to be transmitted : ");

                String str = Console.ReadLine();
                Stream stm = tcpclnt.GetStream();

                ASCIIEncoding asen = new ASCIIEncoding();
                byte[] ba = asen.GetBytes(str);
                Console.WriteLine("Transmitting.....");

                stm.Write(ba, 0, ba.Length);

                byte[] bb = new byte[100];
                int k = stm.Read(bb, 0, 100);

                for (int i = 0; i < k; i++)
                    Console.Write(Convert.ToChar(bb[i]));

                tcpclnt.Close();
            }

            catch (Exception e)
            {
                Console.WriteLine("Error..... " + e.StackTrace);
            }
        }
    }
}

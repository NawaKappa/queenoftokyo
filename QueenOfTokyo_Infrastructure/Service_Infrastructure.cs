﻿using QOT_Domain_Joueur;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;

namespace QueenOfTokyo_Infrastructure
{
    public static class Service_Infrastructure
    {


        /// <summary>
        /// Donne l'adresse ipv4 local du pc
        /// </summary>
        /// <returns>Adresse ip local</returns>
        public static string GetLocalIPAddress()
        {
            IPHostEntry host = Dns.GetHostEntry(Dns.GetHostName());
            foreach (IPAddress ip in host.AddressList)
            {
                if (ip.AddressFamily == AddressFamily.InterNetwork)
                {
                    return ip.ToString();

                }
            }
            throw new ArgumentException("Adresse IP local non trouvé");
        }

        /// <summary>
        /// Converti un string en tab de byte
        /// </summary>
        /// <param name="message">Message en string</param>
        /// <returns>Message en byte</returns>
        public static byte[] StringToByte(string message)
        {
            ASCIIEncoding asen = new ASCIIEncoding();
            return asen.GetBytes(message);
        }

        /// <summary>
        /// Converti un tableau de byte en string, traduis le message
        /// </summary>
        /// <param name="message">Tableau de byte</param>
        /// <returns>Message en chaine de caractère</returns>
        public static string ByteToString(byte[] message)
        {
            StringBuilder res = new StringBuilder();
            foreach (byte b in message)
            {
                res.Append(Convert.ToChar(b).ToString());
            }
            return res.ToString().Replace("\0", string.Empty);
        }


        public static bool PredicateJoueurNomAvatar(string joueur, string avatar)
        {
            if (joueur == avatar)
                return true;
            else
                return false;
        }

        public static void ClearConnexion(object sender, EventArgs e)
        {
            if (HostServer.amIHost)
                HostServer.TCPIPCleaner();
            Client.CleanClient();
        }
    }
}
